rm -r ./publish

# Build publish Cross64
cd src/Cross64
dotnet publish -c Win86 -r win-x86 -p:PublishSingleFile=true --self-contained || exit
dotnet publish -c Win64 -r win-x64 -p:PublishSingleFile=true --self-contained || exit
dotnet publish -c Lin64 -r linux-x64 -p:PublishSingleFile=true --self-contained || exit
dotnet publish -c Osx64 -r osx-x64 -p:PublishSingleFile=true --self-contained || exit
cd ../..

# Move publish stuff to remote folder
mkdir -p publish
cp -r ./bin/publish/net5.0/win-x86/publish ./publish/win-x86/
cp -r ./bin/publish/net5.0/win-x64/publish ./publish/win-x64/
cp -r ./bin/publish/net5.0/linux-x64/publish ./publish/linux-x64/
cp -r ./bin/publish/net5.0/osx-x64/publish ./publish/osx-x64/
rm -r ./bin/publish

# Build/Clone mods
dotnet build Cross64.sln -c Release || exit
for dir in ./src/Mods/*; do
    mkdir -p ./publish/win-x86/plugins
    mkdir -p ./publish/win-x64/plugins
    mkdir -p ./publish/linux-x64/plugins
    mkdir -p ./publish/osx-x64/plugins

    cp $dir/bin/Release/net5.0/*.dll ./publish/win-x86/plugins/
    cp $dir/bin/Release/net5.0/*.dll ./publish/win-x64/plugins/
    cp $dir/bin/Release/net5.0/*.dll ./publish/linux-x64/plugins/
    cp $dir/bin/Release/net5.0/*.dll ./publish/osx-x64/plugins/
done

# Compress everything automatically
cd ./publish/win-x86
7z a -r ../win86 *

cd ../win-x64
7z a -r ../win64 *

cd ../linux-x64
7z a -r ../lin64 *

cd ../osx-x64
7z a -r ../osx64 *
