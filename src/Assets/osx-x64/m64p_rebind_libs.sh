install_name_tool -change /usr/lib/libz.1.dylib @executable_path/emulator/libz.1.dylib mupen64plus.dylib
install_name_tool -change /usr/local/opt/libpng/lib/libpng16.16.dylib @executable_path/emulator/libpng16.16.dylib mupen64plus.dylib
install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/emulator/libSDL2-2.0.0.dylib mupen64plus.dylib
install_name_tool -change /usr/local/opt/freetype/lib/libfreetype.6.dylib @executable_path/emulator/libfreetype.6.dylib mupen64plus.dylib

install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/emulator/libSDL2-2.0.0.dylib mupen64plus-audio-sdl.dylib
install_name_tool -change /usr/local/opt/libsamplerate/lib/libsamplerate.0.dylib @executable_path/emulator/libsamplerate.0.dylib mupen64plus-audio-sdl.dylib

install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/emulator/libSDL2-2.0.0.dylib mupen64plus-input-sdl.dylib

install_name_tool -change /usr/lib/libz.1.dylib @executable_path/emulator/libz.1.dylib mupen64plus-video-rice.dylib
install_name_tool -change /usr/local/opt/libpng/lib/libpng16.16.dylib @executable_path/emulator/libpng16.16.dylib mupen64plus-video-rice.dylib
install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/emulator/libSDL2-2.0.0.dylib mupen64plus-video-rice.dylib

install_name_tool -change /usr/lib/libz.1.dylib @executable_path/emulator/libz.1.dylib mupen64plus-video-glide64mk2.dylib
install_name_tool -change /usr/local/opt/libpng/lib/libpng16.16.dylib @executable_path/emulator/libpng16.16.dylib mupen64plus-video-glide64mk2.dylib
install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/emulator/libSDL2-2.0.0.dylib mupen64plus-video-glide64mk2.dylib
install_name_tool -change /usr/local/opt/boost/lib/libboost_filesystem-mt.dylib @executable_path/emulator/libboost_filesystem-mt.dylib mupen64plus-video-glide64mk2.dylib
install_name_tool -change /usr/local/opt/boost/lib/libboost_system-mt.dylib @executable_path/emulator/libboost_system-mt.dylib mupen64plus-video-glide64mk2.dylib

install_name_tool -change /usr/lib/libz.1.dylib @executable_path/emulator/libz.1.dylib mupen64plus-video-gliden64.dylib
install_name_tool -change /usr/local/opt/libpng/lib/libpng16.16.dylib @executable_path/emulator/libpng16.16.dylib mupen64plus-video-gliden64.dylib
install_name_tool -change /usr/local/opt/freetype/lib/libfreetype.6.dylib @executable_path/emulator/libfreetype.6.dylib mupen64plus-video-gliden64.dylib

cp /usr/lib/libz.1.dylib ./libz.1.dylib
cp /usr/local/opt/libpng/lib/libpng16.16.dylib ./libpng16.16.dylib
cp /usr/local/lib/libSDL2-2.0.0.dylib ./libSDL2-2.0.0.dylib
cp /usr/local/opt/freetype/lib/libfreetype.6.dylib ./libfreetype.6.dylib
cp /usr/local/opt/libsamplerate/lib/libsamplerate.0.dylib ./libsamplerate.0.dylib
cp /usr/local/opt/boost/lib/libboost_filesystem-mt.dylib ./libboost_filesystem-mt.dylib
cp /usr/local/opt/boost/lib/libboost_system-mt.dylib ./libboost_system-mt.dylib

install_name_tool -change /usr/lib/libSystem.B.dylib @executable_path/emulator/libSystem.B.dylib mupen64plus-audio-sdl.dylib
