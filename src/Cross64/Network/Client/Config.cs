﻿using System;
using Asfw.Network;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossClient
        {
            internal static Client Socket;
            internal static bool Connecting;

            internal static bool WaitingOnLobby
            {
                get => _waitingOnLobby && (Socket?.IsConnected ?? false);
                set => _waitingOnLobby = value;
            }

            private static bool _waitingOnLobby;

            internal static void InitNetwork()
            {
                if (Socket != null) return;

                Socket = new Client((int) ServerPackets.Count, 2048);
                Socket.ConnectionSuccess += Socket_ConnectionSuccess;
                Socket.ConnectionLost += Socket_ConnectionLost;
                Socket.ConnectionFailed += Socket_ConnectionFailed;
                Socket.CrashReport += Socket_CrashReport;

                Socket.ThreadControl = true;
                PacketRouter();
            }

            internal static void Connect(string ip, int port)
            {
                Connecting = true;
                Socket?.Connect(ip, port);
            }

            internal static void DestroyNetwork()
            {
                try
                {
                    Socket?.Dispose();
                }
                catch
                {
                    // Do nothing, get outta here!
                }

                Socket = null;
            }

            private static void Socket_ConnectionSuccess()
            {
                Connecting = false;
            }

            private static void Socket_ConnectionFailed()
            {
                Connecting = false;
            }

            private static void Socket_ConnectionLost()
            {
                Connecting = false;

                Console.WriteLine("Connection to server was lost... Closing netplay plugin!");
                ModLoader.OnlineMod.Dispose();
                ModLoader.OnlineMod = null;
            }

            internal static void Socket_CrashReport(string err)
            {
                Console.WriteLine($"There was a network error -> Report: {err}");
            }
        }
    }
}