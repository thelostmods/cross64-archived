﻿namespace Cross64
{
    internal static partial class Network
    {
        internal enum ClientPackets
        {
            #region In-Game Packets

            /// <summary> Cross64 mod-created (generic) packet </summary>
            CrossPacket,

            /// <summary> Attempt join lobby </summary>
            Connect,

            /// <summary> Leave the lobby </summary>
            Disconnect,

            #endregion

            #region Pre-Game Packets

            /// <summary> Requests the lobbies that exist for lobby browser </summary>
            FetchListing,

            #endregion

            Count
        }

        internal enum ServerPackets
        {
            #region In-Game Packets

            /// <summary> Cross64 mod-created (generic) packet </summary>
            CrossPacket,

            /// <summary>
            /// Sends a notification that the connection was successful.
            /// Comes with extra byte[] storage in case data needs to be passed
            /// as soon as the player joins the lobby (Like passing storage data)
            /// </summary>
            ConnectSuccess,

            /// <summary>
            /// Sends a notification that the connection failed.
            /// Will revert back to the connection screen on client side
            /// </summary>
            ConnectFail,
            
            /// <summary>
            /// Tells other players that someone entered their lobby
            /// </summary>
            PlayerConnected,
            
            /// <summary>
            /// Tells other players that someone left their lobby
            /// </summary>
            PlayerLeft,

            #endregion

            #region Pre-Game Packets

            /// <summary> Sends a list of lobby info for the lobby browser </summary>
            LobbyList,

            #endregion

            Count
        }
    }
}