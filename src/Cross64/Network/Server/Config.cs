﻿using System;
using System.Collections.Generic;
using Asfw.Network;
using Cross64.Runtimes.Modding;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossServer
        {
            internal class PlayerDef
            {
                public NetworkedModBase Lobby;
                public string Name;
                public int PlayerIndex;

                public PlayerDef(NetworkedModBase lobby, string name, int index)
                {
                    Lobby = lobby;
                    Name = name;
                    PlayerIndex = index;
                }
            }

            internal static Server Socket;
            internal static Dictionary<string, NetworkedModBase> Lobbies;
            internal static Dictionary<int, PlayerDef> Players;
            internal static bool Hosting = false;

            internal static void InitNetwork()
            {
                if (Socket != null) return;

                Socket = new Server((int) ClientPackets.Count, 2048);
                Socket.ConnectionReceived += Socket_ConnectionReceived;
                Socket.ConnectionLost += Socket_ConnectionLost;
                Socket.CrashReport += Socket_CrashReport;

                PacketRouter();

                // Init our trackers
                Lobbies = new Dictionary<string, NetworkedModBase>();
                Players = new Dictionary<int, PlayerDef>();
            }

            internal static void DestroyNetwork()
            {
                try
                {
                    Socket?.Dispose();
                }
                catch
                {
                    // Do nothing, get outta here!
                }

                Socket = null;

                // Destroy our trackers
                Lobbies?.Clear();
                Players?.Clear();
                Lobbies = null!;
                Players = null!;
                Hosting = false;
                
                Program.CollectGarbage();
            }

            private static void Socket_ConnectionReceived(int index)
            {
                Console.WriteLine($@"Connection received on index[{index}] - IP[{Socket?.ClientIp(index)}]");
            }

            private static void Socket_ConnectionLost(int index)
            {
                Console.WriteLine($@"Connection lost on index[{index}] - IP[{Socket?.ClientIp(index)}]");

                if (!Players.ContainsKey(index)) return;
                
                var p = Players[index];
                var pLobby = p.Lobby;
                
                // Notify other players in lobby
                SendPlayerLeft(index, p.PlayerIndex);

                // Remove from server copy of lobby
                pLobby.RemovePlayer(p.PlayerIndex);

                // Kill lobby if no one is in it!
                if (pLobby.Players.Count < 1)
                {
                    pLobby.PluginInfo.Instance.Remove(pLobby.Lobby);
                    Lobbies.Remove(pLobby.Lobby);
                    pLobby.Dispose();
                    Program.CollectGarbage();
                }
                
                // Kill the player object
                Players.Remove(index);
            }

            private static void Socket_CrashReport(int index, string err)
            {
                Console.WriteLine($@"There was a network error -> Index[{index}]\nReport: {err}");
            }

            private static void SendToLobby(string lobby, byte[] data)
            {
                foreach (var plyr in Lobbies[lobby].PlayerId.Values)
                    Socket.SendDataTo(plyr, data);
            }

            private static void SendToLobby(int index, byte[] data)
            {
                var lobby = Players[index].Lobby;
                foreach (var plyr in lobby.PlayerId.Values)
                    Socket.SendDataTo(plyr, data);
            }
        }
    }
}