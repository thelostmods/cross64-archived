using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NintendoAllStarDairantoSmashBrothers : ApiBase
    {
        public override string GameName => "Nintendo All-Star! Dairantō Smash Brothers";
        public override GameID GameID => GameID.NintendoAllStarDairantoSmashBrothers;

        public NintendoAllStarDairantoSmashBrothers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
