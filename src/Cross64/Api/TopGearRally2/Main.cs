using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TopGearRally2 : ApiBase
    {
        public override string GameName => "Top Gear Rally 2";
        public override GameID GameID => GameID.TopGearRally2;

        public TopGearRally2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
