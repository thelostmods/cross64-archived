using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Battltanx : ApiBase
    {
        public override string GameName => "Battletanx";
        public override GameID GameID => GameID.Battltanx;

        public Battltanx()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
