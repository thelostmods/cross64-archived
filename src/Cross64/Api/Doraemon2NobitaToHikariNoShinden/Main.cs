using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Doraemon2NobitaToHikariNoShinden : ApiBase
    {
        public override string GameName => "Doraemon 2: Nobita To Hikari No Shinden";
        public override GameID GameID => GameID.Doraemon2NobitaToHikariNoShinden;

        public Doraemon2NobitaToHikariNoShinden()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
