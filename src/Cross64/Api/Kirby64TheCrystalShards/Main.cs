using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Kirby64TheCrystalShards : ApiBase
    {
        public override string GameName => "Kirby 64: The Crystal Shards";
        public override GameID GameID => GameID.Kirby64TheCrystalShards;

        public Kirby64TheCrystalShards()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
