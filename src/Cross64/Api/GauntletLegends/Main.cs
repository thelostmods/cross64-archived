using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GauntletLegends : ApiBase
    {
        public override string GameName => "Gauntlet Legends";
        public override GameID GameID => GameID.GauntletLegends;

        public GauntletLegends()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
