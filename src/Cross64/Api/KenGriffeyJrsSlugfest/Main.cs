using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KenGriffeyJrsSlugfest : ApiBase
    {
        public override string GameName => "Ken Griffey Jr.'s Slugfest";
        public override GameID GameID => GameID.KenGriffeyJrsSlugfest;

        public KenGriffeyJrsSlugfest()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
