using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AllStarTennis99 : ApiBase
    {
        public override string GameName => "All-Star Tennis '99";
        public override GameID GameID => GameID.AllStarTennis99;

        public AllStarTennis99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
