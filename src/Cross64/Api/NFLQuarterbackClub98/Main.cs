using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLQuarterbackClub98 : ApiBase
    {
        public override string GameName => "NFL Quarterback Club '98";
        public override GameID GameID => GameID.NFLQuarterbackClub98;

        public NFLQuarterbackClub98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
