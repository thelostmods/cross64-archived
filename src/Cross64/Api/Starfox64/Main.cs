using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Starfox64 : ApiBase
    {
        public override string GameName => "Starfox 64";
        public override GameID GameID => GameID.Starfox64;

        public Starfox64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
