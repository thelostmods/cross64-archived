using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaddenNFL2000 : ApiBase
    {
        public override string GameName => "Madden NFL 2000";
        public override GameID GameID => GameID.MaddenNFL2000;

        public MaddenNFL2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
