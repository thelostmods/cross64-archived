using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class InternationalSuperstarSoccer64 : ApiBase
    {
        public override string GameName => "International Superstar Soccer 64";
        public override GameID GameID => GameID.InternationalSuperstarSoccer64;

        public InternationalSuperstarSoccer64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
