using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class SuperMario64
    {
        public class RuntimeDef
        {
            private Emulator.Pointer addrCurProf = Addresses[(int)AddressType.CurProfile];
            private Emulator.Pointer addrCurScene = Addresses[(int)AddressType.CurScene];
            private Emulator.Pointer addrPaused = Addresses[(int)AddressType.Paused];
            private Emulator.Pointer addrStars = Addresses[(int)AddressType.Stars];

            public ProfileType CurrentProfile => (ProfileType)addrCurProf.U8 - 1;
            public ushort CurrentScene => addrCurScene.U16;
            public bool IsPaused => addrPaused.U16 == 1;

            public ushort StarCount
            {
                get => addrStars.U16;
                set => addrStars.U16 = value;
            }
        }
    }
}