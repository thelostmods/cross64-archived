using System;
using System.Collections.Generic;
using System.Linq;
using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class SuperMario64
    {
        public class PlayerDef
        {
            private Dictionary<int, byte[]> _anims = new ();

            private Emulator.Pointer _instance = Addresses[(int) AddressType.Player];

            internal PlayerDef()
            {
                // Load animations file
                var raw = Asfw.IO.Compression.DecompressBytes(Resources.Sm64_Anims);
                using var bs = new Asfw.ByteStream(raw);
                while (bs.Head < raw.Length)
                {
                    _anims?.Add(bs.ReadInt32(), bs.ReadBytes());
                }
            }

            internal void Destroy()
            {
                _anims?.Clear();
                _anims = null;
            }

            public bool Exists => _instance.U32 != 0;

            public ushort AnimationFrame
            {
                get => _instance.ReadP16(0x40);
                set => _instance.WriteP16(0x40, value);
            }

            public ushort AnimationId
            {
                get => _instance.ReadP16(0x38);
                set => _instance.WriteP16(0x38, value);
            }

            public byte[] AnimationBlockData
            {
                get => _instance.ReadPB(0x40, 8);
                set => _instance.WritePB(0x40, value);
            }

            public byte Cap => 0;

            public Utility.Vector3 Position
            {
                get => new Utility.Vector3(_instance.ReadPB(0xa0, 12));
                set => _instance.WritePB(0xa0, value.ToArray());
            }

            public byte[] PositionRaw
            {
                get => _instance.ReadPB(0xa0, 12);
                set => _instance.WritePB(0xa0, value);
            }

            public float PosX
            {
                get => _instance.ReadPF(0xa0);
                set => _instance.WritePF(0xa0, value);
            }

            public float PosY
            {
                get => _instance.ReadPF(0xa4);
                set => _instance.WritePF(0xa4, value);
            }

            public float PosZ
            {
                get => _instance.ReadPF(0xa8);
                set => _instance.WritePF(0xa8, value);
            }

            public Utility.Vector3 Rotation
            {
                get => new Utility.Vector3(_instance.ReadPB(0xd0, 12));
                set => _instance.WritePB(0xd0, value.ToArray());
            }

            public byte[] RotationRaw
            {
                get => _instance.ReadPB(0xd0, 12);
                set => _instance.WritePB(0xd0, value);
            }

            public float RotX
            {
                get => _instance.ReadPF(0xd0);
                set => _instance.WritePF(0xd0, value);
            }

            public float RotY
            {
                get => _instance.ReadPF(0xd4);
                set => _instance.WritePF(0xd4, value);
            }

            public float RotZ
            {
                get => _instance.ReadPF(0xd8);
                set => _instance.WritePF(0xd8, value);
            }

            public float Translucency
            {
                get => _instance.ReadPF(0x017c);
                set => _instance.WritePF(0x017c, value);
            }

            public bool Visible
            {
                get => _instance.ReadP8(0x02) == 0x21;
                set => _instance.WriteP8(0x02, (byte) (value ? 0x21 : 0x20));
            }

            public ushort YOffset
            {
                get => _instance.ReadP16(0x3a);
                set => _instance.WriteP16(0x3a, value);
            }

            public byte[] AnimationRaw(int id) => _anims?[id];

            internal void OnTick()
            {
                // Attempt to cache any unknown animations
                var id = AnimationId;
                if (_anims != null && !_anims.ContainsKey(id))
                    _anims.Add(id, GetAnim());
            }

            private byte[] GetAnim()
            {
                var ptrData = (_instance.Deref + 0x3c).Deref;
                var size = (int) (ptrData + 0x14).U32;
                var ptrDeref = ptrData & 0xffffff;
                var ret = ptrData.ReadB(size);

                var x0C = BitConverter.GetBytes(BitConverter.ToUInt32(ret, 0x0C) - ptrDeref);
                var x10 = BitConverter.GetBytes(BitConverter.ToUInt32(ret, 0x10) - ptrDeref);

                Buffer.BlockCopy(x0C, 0, ret, 0x0c, 4);
                Buffer.BlockCopy(x10, 0, ret, 0x10, 4);

                return ret;
            }
        }
    }
}