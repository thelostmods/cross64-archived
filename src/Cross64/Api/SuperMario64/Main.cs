﻿using System;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperMario64 : ApiBase
    {
        public override string GameName => "Super Mario 64";
        public override GameID GameID => GameID.SuperMario64;
        public GameVersion Version { get; private set; }
        public HackType Hack { get; private set; }

        public PlayerDef Player { get; private set; }
        public RuntimeDef Runtime { get; private set; }
        public BufferObject[] Save { get; private set; }

        public int SaveDataSize;

        internal override void Initialize()
        {
            DetectHackType();

            Version = Emulator.Cartridge.Country switch
            {
                "J" => GameVersion.JP_1_0,
                "P" => GameVersion.PAL_1_0,
                "E" => GameVersion.USA_1_0,
                _ => throw new Exception("[Rom corruption]: No country code exists")
            };
            SetVersion(Version);

            Player = new PlayerDef();
            Runtime = new RuntimeDef();
            Save = new[]
            {
                new BufferObject(Addresses[(int) AddressType.FileA], SaveDataSize),
                new BufferObject(Addresses[(int) AddressType.FileB], SaveDataSize),
                new BufferObject(Addresses[(int) AddressType.FileC], SaveDataSize),
                new BufferObject(Addresses[(int) AddressType.FileD], SaveDataSize)
            };
        }

        internal override void Destroy()
        {
            Player?.Destroy();
        }

        public override void OnFirstFrame()
        {
        }

        public override void OnTick(uint frame)
        {
            if (Player == null || !Player.Exists) return;
            Player.OnTick();
        }
    }
}