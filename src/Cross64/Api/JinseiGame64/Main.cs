using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JinseiGame64 : ApiBase
    {
        public override string GameName => "Jinsei Game 64";
        public override GameID GameID => GameID.JinseiGame64;

        public JinseiGame64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
