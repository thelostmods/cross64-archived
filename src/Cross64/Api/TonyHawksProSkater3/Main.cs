using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TonyHawksProSkater3 : ApiBase
    {
        public override string GameName => "Tony Hawk's Pro Skater 3";
        public override GameID GameID => GameID.TonyHawksProSkater3;

        public TonyHawksProSkater3()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
