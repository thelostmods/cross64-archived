using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GoemonsGreatAdventure : ApiBase
    {
        public override string GameName => "Goemon's Great Adventure";
        public override GameID GameID => GameID.GoemonsGreatAdventure;

        public GoemonsGreatAdventure()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
