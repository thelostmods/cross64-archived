using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoWorldSoccerWorldCupFrance98 : ApiBase
    {
        public override string GameName => "Jikkyō World Soccer: World Cup France '98";
        public override GameID GameID => GameID.JikkyoWorldSoccerWorldCupFrance98;

        public JikkyoWorldSoccerWorldCupFrance98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
