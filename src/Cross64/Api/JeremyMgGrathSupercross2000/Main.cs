using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JeremyMgGrathSupercross2000 : ApiBase
    {
        public override string GameName => "Jeremy McGrath Supercross 2000";
        public override GameID GameID => GameID.JeremyMgGrathSupercross2000;

        public JeremyMgGrathSupercross2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
