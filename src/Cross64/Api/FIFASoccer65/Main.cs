using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FIFASoccer65 : ApiBase
    {
        public override string GameName => "FIFA Soccer 64";
        public override GameID GameID => GameID.FIFASoccer65;

        public FIFASoccer65()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
