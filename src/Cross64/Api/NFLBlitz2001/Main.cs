using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLBlitz2001 : ApiBase
    {
        public override string GameName => "NFL Blitz 2001";
        public override GameID GameID => GameID.NFLBlitz2001;

        public NFLBlitz2001()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
