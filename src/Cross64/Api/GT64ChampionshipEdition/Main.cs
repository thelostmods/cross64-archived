using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GT64ChampionshipEdition : ApiBase
    {
        public override string GameName => "GT 64: Championship Edition";
        public override GameID GameID => GameID.GT64ChampionshipEdition;

        public GT64ChampionshipEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
