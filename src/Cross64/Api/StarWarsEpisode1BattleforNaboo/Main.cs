using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarWarsEpisode1BattleforNaboo : ApiBase
    {
        public override string GameName => "Star Wars: Episode 1 Battle for Naboo";
        public override GameID GameID => GameID.StarWarsEpisode1BattleforNaboo;

        public StarWarsEpisode1BattleforNaboo()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
