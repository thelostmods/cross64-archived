using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SaikyoHabuShogi : ApiBase
    {
        public override string GameName => "Saikyō Habu Shōgi";
        public override GameID GameID => GameID.SaikyoHabuShogi;

        public SaikyoHabuShogi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
