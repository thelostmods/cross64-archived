﻿using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ConkersBadFurDay : ApiBase
    {
        public override string GameName => "Conker's Bad Fur Day";
        public override GameID GameID => GameID.ConkersBadFurDay;

        public ConkersBadFurDay()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}