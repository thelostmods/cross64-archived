using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLQuarterbackClub2000 : ApiBase
    {
        public override string GameName => "NFL Quarterback Club 2001";
        public override GameID GameID => GameID.NFLQuarterbackClub2000;

        public NFLQuarterbackClub2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
