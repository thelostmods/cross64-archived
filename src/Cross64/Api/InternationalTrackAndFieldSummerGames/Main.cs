using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class InternationalTrackAndFieldSummerGames : ApiBase
    {
        public override string GameName => "International Track & Field: Summer Games";
        public override GameID GameID => GameID.InternationalTrackAndFieldSummerGames;

        public InternationalTrackAndFieldSummerGames()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
