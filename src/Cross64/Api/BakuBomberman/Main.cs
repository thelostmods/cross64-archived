using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BakuBomberman : ApiBase
    {
        public override string GameName => "Baku Bomberman";
        public override GameID GameID => GameID.BakuBomberman;

        public BakuBomberman()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
