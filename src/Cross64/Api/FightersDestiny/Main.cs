using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FightersDestiny : ApiBase
    {
        public override string GameName => "Fighters Destiny";
        public override GameID GameID => GameID.FightersDestiny;

        public FightersDestiny()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
