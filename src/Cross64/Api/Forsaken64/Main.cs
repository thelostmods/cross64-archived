using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Forsaken64 : ApiBase
    {
        public override string GameName => "Forsaken 64";
        public override GameID GameID => GameID.Forsaken64;

        public Forsaken64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
