using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PokemonStadium : ApiBase
    {
        public override string GameName => "Pokémon Stadium";
        public override GameID GameID => GameID.PokemonStadium;

        public PokemonStadium()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
