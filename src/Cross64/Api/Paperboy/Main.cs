using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Paperboy : ApiBase
    {
        public override string GameName => "Paperboy";
        public override GameID GameID => GameID.Paperboy;

        public Paperboy()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
