using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NamcoMuseum64 : ApiBase
    {
        public override string GameName => "Namco Museum 64";
        public override GameID GameID => GameID.NamcoMuseum64;

        public NamcoMuseum64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
