using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Jeopardy : ApiBase
    {
        public override string GameName => "Jeopardy!";
        public override GameID GameID => GameID.Jeopardy;

        public Jeopardy()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
