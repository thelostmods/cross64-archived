using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TopGearHyperBike : ApiBase
    {
        public override string GameName => "Top Gear Hyper Bike";
        public override GameID GameID => GameID.TopGearHyperBike;

        public TopGearHyperBike()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
