using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Spiderman : ApiBase
    {
        public override string GameName => "Spiderman";
        public override GameID GameID => GameID.Spiderman;

        public Spiderman()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
