using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CustomRoboV2 : ApiBase
    {
        public override string GameName => "Custom Robo V2";
        public override GameID GameID => GameID.CustomRoboV2;

        public CustomRoboV2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
