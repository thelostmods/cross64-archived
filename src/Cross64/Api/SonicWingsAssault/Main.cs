using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SonicWingsAssault : ApiBase
    {
        public override string GameName => "Sonic Wings Assault";
        public override GameID GameID => GameID.SonicWingsAssault;

        public SonicWingsAssault()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
