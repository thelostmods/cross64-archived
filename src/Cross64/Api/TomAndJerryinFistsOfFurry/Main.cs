using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TomAndJerryinFistsOfFurry : ApiBase
    {
        public override string GameName => "Tom & Jerry in Fists Of Furry";
        public override GameID GameID => GameID.TomAndJerryinFistsOfFurry;

        public TomAndJerryinFistsOfFurry()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
