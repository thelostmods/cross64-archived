using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class XenaWarriorPrincess : ApiBase
    {
        public override string GameName => "Xena Warrior Princess";
        public override GameID GameID => GameID.XenaWarriorPrincess;

        public XenaWarriorPrincess()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
