using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PDUoutoramanBatoruKorekushon64 : ApiBase
    {
        public override string GameName => "PD Uoutoraman Batoru Korekushon 64";
        public override GameID GameID => GameID.PDUoutoramanBatoruKorekushon64;

        public PDUoutoramanBatoruKorekushon64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
