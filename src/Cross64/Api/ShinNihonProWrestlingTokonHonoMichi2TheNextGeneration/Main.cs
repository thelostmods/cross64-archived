using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ShinNihonProWrestlingTokonHonoMichi2TheNextGeneration : ApiBase
    {
        public override string GameName => "Shin Nihon Pro Wrestling: Tōkon Honō Michi 2, The Next Generation";
        public override GameID GameID => GameID.ShinNihonProWrestlingTokonHonoMichi2TheNextGeneration;

        public ShinNihonProWrestlingTokonHonoMichi2TheNextGeneration()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
