using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SCARS : ApiBase
    {
        public override string GameName => "S.C.A.R.S.";
        public override GameID GameID => GameID.SCARS;

        public SCARS()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
