using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAInTheZone2000 : ApiBase
    {
        public override string GameName => "NBA In The Zone 2000";
        public override GameID GameID => GameID.NBAInTheZone2000;

        public NBAInTheZone2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
