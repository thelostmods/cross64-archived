using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoWorldSoccer3 : ApiBase
    {
        public override string GameName => "Jikkyō World Soccer 3";
        public override GameID GameID => GameID.JikkyoWorldSoccer3;

        public JikkyoWorldSoccer3()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
