using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Famista64 : ApiBase
    {
        public override string GameName => "Famista 64";
        public override GameID GameID => GameID.Famista64;

        public Famista64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
