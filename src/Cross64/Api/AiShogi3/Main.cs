using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AiShogi3 : ApiBase
    {
        public override string GameName => "AI Shogi 3";
        public override GameID GameID => GameID.AiShogi3;

        public AiShogi3()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
