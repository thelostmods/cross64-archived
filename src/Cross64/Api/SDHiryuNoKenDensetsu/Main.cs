using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SDHiryuNoKenDensetsu : ApiBase
    {
        public override string GameName => "SD Hiryu No Ken Densetsu";
        public override GameID GameID => GameID.SDHiryuNoKenDensetsu;

        public SDHiryuNoKenDensetsu()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
