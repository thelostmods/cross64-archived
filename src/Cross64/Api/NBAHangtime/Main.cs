using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAHangtime : ApiBase
    {
        public override string GameName => "NBA Hangtime";
        public override GameID GameID => GameID.NBAHangtime;

        public NBAHangtime()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
