using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChokukanNightProYakyuKing2 : ApiBase
    {
        public override string GameName => "Chōkukan Night Pro Yakyu King 2";
        public override GameID GameID => GameID.ChokukanNightProYakyuKing2;

        public ChokukanNightProYakyuKing2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
