using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarionoFotopi : ApiBase
    {
        public override string GameName => "Mario no Fotopī";
        public override GameID GameID => GameID.MarionoFotopi;

        public MarionoFotopi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
