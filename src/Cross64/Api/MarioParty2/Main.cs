using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioParty2 : ApiBase
    {
        public override string GameName => "Mario Party 2";
        public override GameID GameID => GameID.MarioParty2;

        public MarioParty2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
