using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HydroThunder : ApiBase
    {
        public override string GameName => "Hydro Thunder";
        public override GameID GameID => GameID.HydroThunder;

        public HydroThunder()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
