using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FIFA99 : ApiBase
    {
        public override string GameName => "FIFA '99";
        public override GameID GameID => GameID.FIFA99;

        public FIFA99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
