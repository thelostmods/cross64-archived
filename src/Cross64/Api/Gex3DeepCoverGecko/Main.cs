using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Gex3DeepCoverGecko : ApiBase
    {
        public override string GameName => "Gex 3: Deep Cover Gecko";
        public override GameID GameID => GameID.Gex3DeepCoverGecko;

        public Gex3DeepCoverGecko()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
