using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Dezaemon3D : ApiBase
    {
        public override string GameName => "Dezaemon 3D";
        public override GameID GameID => GameID.Dezaemon3D;

        public Dezaemon3D()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
