using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperSpeedRace64 : ApiBase
    {
        public override string GameName => "Super Speed Race 64";
        public override GameID GameID => GameID.SuperSpeedRace64;

        public SuperSpeedRace64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
