using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Castlevania : ApiBase
    {
        public override string GameName => "Castlevania";
        public override GameID GameID => GameID.Castlevania;

        public Castlevania()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
