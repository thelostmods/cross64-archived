using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WaveRace64 : ApiBase
    {
        public override string GameName => "Wave Race 64";
        public override GameID GameID => GameID.WaveRace64;

        public WaveRace64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
