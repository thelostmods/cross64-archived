using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAInTheZone99 : ApiBase
    {
        public override string GameName => "NBA In The Zone '99";
        public override GameID GameID => GameID.NBAInTheZone99;

        public NBAInTheZone99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
