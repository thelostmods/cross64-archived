using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AllStarBaseball99 : ApiBase
    {
        public override string GameName => "All-Star Baseball '99";
        public override GameID GameID => GameID.AllStarBaseball99;

        public AllStarBaseball99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
