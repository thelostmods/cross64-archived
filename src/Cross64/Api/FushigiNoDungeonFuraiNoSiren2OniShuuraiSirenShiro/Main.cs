using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FushigiNoDungeonFuraiNoSiren2OniShuuraiSirenShiro : ApiBase
    {
        public override string GameName => "Fushigi No Dungeon: Furai No Siren 2 Oni Shuurai! Shiren Shiro!";
        public override GameID GameID => GameID.FushigiNoDungeonFuraiNoSiren2OniShuuraiSirenShiro;

        public FushigiNoDungeonFuraiNoSiren2OniShuuraiSirenShiro()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
