using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioGolf : ApiBase
    {
        public override string GameName => "Mario Golf";
        public override GameID GameID => GameID.MarioGolf;

        public MarioGolf()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
