using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RobotPonkottsu64NanatsunoUminoCaramel : ApiBase
    {
        public override string GameName => "Robot Ponkottsu 64: Nanatsu no Umi no Caramel";
        public override GameID GameID => GameID.RobotPonkottsu64NanatsunoUminoCaramel;

        public RobotPonkottsu64NanatsunoUminoCaramel()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
