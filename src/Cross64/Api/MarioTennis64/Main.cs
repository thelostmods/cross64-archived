using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioTennis64 : ApiBase
    {
        public override string GameName => "Mario Tennis 64";
        public override GameID GameID => GameID.MarioTennis64;

        public MarioTennis64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
