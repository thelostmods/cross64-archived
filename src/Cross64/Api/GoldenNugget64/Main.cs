using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GoldenNugget64 : ApiBase
    {
        public override string GameName => "Golden Nugget 64";
        public override GameID GameID => GameID.GoldenNugget64;

        public GoldenNugget64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
