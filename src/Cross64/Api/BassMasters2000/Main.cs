using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BassMasters2000 : ApiBase
    {
        public override string GameName => "Bass Masters 2000";
        public override GameID GameID => GameID.BassMasters2000;

        public BassMasters2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
