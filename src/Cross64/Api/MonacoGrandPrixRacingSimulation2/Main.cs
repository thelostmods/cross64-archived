using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MonacoGrandPrixRacingSimulation2 : ApiBase
    {
        public override string GameName => "Monaco Grand Prix Racing Simulation 2";
        public override GameID GameID => GameID.MonacoGrandPrixRacingSimulation2;

        public MonacoGrandPrixRacingSimulation2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
