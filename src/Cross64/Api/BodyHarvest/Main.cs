using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BodyHarvest : ApiBase
    {
        public override string GameName => "Body Harvest";
        public override GameID GameID => GameID.BodyHarvest;

        public BodyHarvest()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
