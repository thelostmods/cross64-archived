using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoGIStable : ApiBase
    {
        public override string GameName => "Jikkyō GI Stable";
        public override GameID GameID => GameID.JikkyoGIStable;

        public JikkyoGIStable()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
