using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Roadsters : ApiBase
    {
        public override string GameName => "Roadsters";
        public override GameID GameID => GameID.Roadsters;

        public Roadsters()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
