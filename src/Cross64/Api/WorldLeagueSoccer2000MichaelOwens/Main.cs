using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WorldLeagueSoccer2000MichaelOwens : ApiBase
    {
        public override string GameName => "World League Soccer 2000, Michael Owen's";
        public override GameID GameID => GameID.WorldLeagueSoccer2000MichaelOwens;

        public WorldLeagueSoccer2000MichaelOwens()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
