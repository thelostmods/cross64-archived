using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoPowerfulProYakyu2000 : ApiBase
    {
        public override string GameName => "Jikkyō Powerful Pro Yakyū 2000";
        public override GameID GameID => GameID.JikkyoPowerfulProYakyu2000;

        public JikkyoPowerfulProYakyu2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
