using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Starfox64E : ApiBase
    {
        public override string GameName => "Starfox 64 USA";
        public override GameID GameID => GameID.Starfox64E;

        public Starfox64E()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
