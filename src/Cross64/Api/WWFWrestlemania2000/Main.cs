using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WWFWrestlemania2000 : ApiBase
    {
        public override string GameName => "WWF Wrestlemania 2000";
        public override GameID GameID => GameID.WWFWrestlemania2000;

        public WWFWrestlemania2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
