using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PokemonSnap : ApiBase
    {
        public override string GameName => "Pokémon Snap";
        public override GameID GameID => GameID.PokemonSnap;

        public PokemonSnap()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
