using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Ready2RumbleBoxingRound2 : ApiBase
    {
        public override string GameName => "Ready 2 Rumble Boxing: Round 2";
        public override GameID GameID => GameID.Ready2RumbleBoxingRound2;

        public Ready2RumbleBoxingRound2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
