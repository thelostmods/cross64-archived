using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TransformersBeastWarsTransmetals : ApiBase
    {
        public override string GameName => "Transformers: Beast Wars Transmetals";
        public override GameID GameID => GameID.TransformersBeastWarsTransmetals;

        public TransformersBeastWarsTransmetals()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
