using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BottomOfThe9th : ApiBase
    {
        public override string GameName => "Bottom of the 9th";
        public override GameID GameID => GameID.BottomOfThe9th;

        public BottomOfThe9th()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
