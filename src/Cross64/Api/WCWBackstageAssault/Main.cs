using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WCWBackstageAssault : ApiBase
    {
        public override string GameName => "WCW Backstage Assault";
        public override GameID GameID => GameID.WCWBackstageAssault;

        public WCWBackstageAssault()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
