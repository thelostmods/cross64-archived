using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BrunswickCircuitProBowling : ApiBase
    {
        public override string GameName => "Brunswick Circuit Pro Bowling";
        public override GameID GameID => GameID.BrunswickCircuitProBowling;

        public BrunswickCircuitProBowling()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
