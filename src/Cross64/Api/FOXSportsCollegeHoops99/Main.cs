using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FOXSportsCollegeHoops99 : ApiBase
    {
        public override string GameName => "FOX Sports College Hoops '99";
        public override GameID GameID => GameID.FOXSportsCollegeHoops99;

        public FOXSportsCollegeHoops99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
