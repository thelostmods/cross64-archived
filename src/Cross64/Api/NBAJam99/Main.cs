using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAJam99 : ApiBase
    {
        public override string GameName => "NBA Jam '99";
        public override GameID GameID => GameID.NBAJam99;

        public NBAJam99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
