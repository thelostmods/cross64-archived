using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Vigilante8SecondOffense : ApiBase
    {
        public override string GameName => "Vigilante 8: Second Offense";
        public override GameID GameID => GameID.Vigilante8SecondOffense;

        public Vigilante8SecondOffense()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
