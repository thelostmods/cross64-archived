using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Gex64EntertheGecko : ApiBase
    {
        public override string GameName => "Gex 64: Enter the Gecko";
        public override GameID GameID => GameID.Gex64EntertheGecko;

        public Gex64EntertheGecko()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
