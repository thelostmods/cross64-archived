using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AutomobiliLamborghini : ApiBase
    {
        public override string GameName => "Automobili Lamborghini";
        public override GameID GameID => GameID.AutomobiliLamborghini;

        public AutomobiliLamborghini()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
