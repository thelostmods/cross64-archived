using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MissionImpossible : ApiBase
    {
        public override string GameName => "Mission: Impossible";
        public override GameID GameID => GameID.MissionImpossible;

        public MissionImpossible()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
