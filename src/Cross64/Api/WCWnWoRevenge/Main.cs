using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WCWnWoRevenge : ApiBase
    {
        public override string GameName => "WCW/nWo Revenge";
        public override GameID GameID => GameID.WCWnWoRevenge;

        public WCWnWoRevenge()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
