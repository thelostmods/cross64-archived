using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KingHill64ExtremeSnowboarding : ApiBase
    {
        public override string GameName => "King Hill 64: Extreme Snowboarding";
        public override GameID GameID => GameID.KingHill64ExtremeSnowboarding;

        public KingHill64ExtremeSnowboarding()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
