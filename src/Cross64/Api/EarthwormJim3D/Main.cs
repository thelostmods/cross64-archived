using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class EarthwormJim3D : ApiBase
    {
        public override string GameName => "Earthworm Jim 3D";
        public override GameID GameID => GameID.EarthwormJim3D;

        public EarthwormJim3D()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
