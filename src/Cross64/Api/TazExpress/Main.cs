using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TazExpress : ApiBase
    {
        public override string GameName => "Taz Express";
        public override GameID GameID => GameID.TazExpress;

        public TazExpress()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
