﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public class GlobalDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Global];

            public uint GetButtonState()
            {
                return _instance.ReadP16(0x14);
            }

            public void SetButtonState(ushort value)
            {
                _instance.WriteP16(0x14, value);
            }

            public bool IsButtonPressed(ushort value)
            {
                return (_instance.ReadP16(0x14) & value) != 0;
            }

            public bool IsButtonPressed(params Emulator.ButtonType[] values)
            {
                var curState = _instance.ReadP16(0x14);
                foreach (var state in values)
                    if ((curState & (uint) state) != 0)
                        return true;
                return false;
            }

            public void PressButton(params Emulator.ButtonType[] values)
            {
                var curState = _instance.ReadP16(0x14);
                foreach (var state in values)
                    curState |= (ushort) state;
                _instance.WriteP16(0x14, curState);
            }

            public void UnPressButton(params Emulator.ButtonType[] values)
            {
                var curState = _instance.ReadP16(0x14);
                foreach (var state in values)
                    curState &= (ushort) ~state;
                _instance.WriteP16(0x14, curState);
            }
        }
    }
}