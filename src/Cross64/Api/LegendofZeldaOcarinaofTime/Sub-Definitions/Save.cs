﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public class SaveDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Save];

            public short HeartContainers
            {
                get => (short) (_instance.Read16(0x002e) / 0x10);
                set => _instance.Write16((ushort) (value * 0x10), 0x002e);
            }

            public short Health
            {
                get => (short) _instance.Read16(0x0030);
                set => _instance.Write16((ushort) value, 0x0030);
            }

            public MagicType MagicMeterSize
            {
                get => (MagicType) _instance.Read8(0x0032);
                set
                {
                    _instance.Write8((byte) value, 0x0032);

                    switch (value)
                    {
                        case MagicType.None:
                            _instance.Write8(0, 0x003a);
                            _instance.Write8(0, 0x003c);
                            _instance.Write16((ushort) MagicType.None, 0x13f4);
                            Magic = 0;
                            break;

                        case MagicType.Normal:
                            _instance.Write8(1, 0x003a);
                            _instance.Write8(0, 0x003c);
                            _instance.Write16((ushort) MagicType.Normal, 0x13f4);
                            break;

                        case MagicType.Extended:
                            _instance.Write8(1, 0x003a);
                            _instance.Write8(1, 0x003c);
                            _instance.Write16((ushort) MagicType.Extended, 0x13f4);
                            break;
                    }
                }
            }

            public byte Magic
            {
                get => _instance.Read8(0x0033);
                set => _instance.Write8(value, 0x0033);
            }

            public short Rupees
            {
                get => (short) _instance.Read16(0x0034);
                set => _instance.Write16((ushort) value, 0x0034);
            }

            public short NaviTimer
            {
                get => (short) _instance.Read16(0x0038);
                set => _instance.Write16((ushort) value, 0x0038);
            }
        }
    }
}