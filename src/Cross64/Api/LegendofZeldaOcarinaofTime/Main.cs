﻿using System;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime : ApiBase
    {
        public override string GameName => "The Legend of Zelda: Ocarina of Time";
        public override GameID GameID => GameID.LegendofZeldaOcarinaofTime;
        public GameVersion Version { get; private set; }

        public GlobalDef Global { get; private set; }
        public PlayerDef Player { get; private set; }
        public RuntimeDef Runtime { get; private set; }
        public SaveDef Save { get; private set; }

        internal override void Initialize()
        {
            Version = (Emulator.Cartridge.Country + Emulator.Cartridge.Revision) switch
            {
                "J0" => GameVersion.USA_JP_1_0,
                "E0" => GameVersion.USA_JP_1_0,
                _ => throw new Exception("[Rom corruption]: No country code exists")
            };
            SetVersion(Version);

            Global = new GlobalDef();
            Player = new PlayerDef();
            Runtime = new RuntimeDef();
            Save = new SaveDef();
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
            if (Player == null || !Player.Exists) return;
            Player.OnTick();
        }
    }
}