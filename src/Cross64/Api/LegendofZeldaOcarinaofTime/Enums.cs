﻿namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public enum ActorType : byte
        {
            Switch,
            Backgrounds,
            Player,
            Bomb,
            Npc,
            Enemy,
            Prop2,
            ItemAction,
            Misc,
            Boss,
            Door,
            Chest
        }

        public enum SceneType
        {
            InsideTheDekuTree,
            DodongosCavern,
            InsideJabuJabusBelly,
            ForestTemple,
            FireTemple,
            WaterTemple,
            SpiritTemple,
            ShadowTemple,
            BottomOfTheWell,
            IceCavern,
            GanonsTower,
            GerudoTrainingGroungs,
            ThievesHideout,
            InsideGanonsCastle,
            GanonsTowerCollapsing,
            InsideGanonsCastleCollapsing,
            TreasureBoxShop,
            GohmasLair,
            KingDodongosLair,
            BarinadesLair,
            PhantomGanonsLair,
            VolvagiasLair,
            MorphasLair,
            TwinrovasLair,
            BongoBongosLair,
            GanondorfsLair,
            TowerCollapseExterior,
            MarketEntranceChildDay,
            MarketEntranceChildNight,
            MarketEntranceAdult,
            BackAlleyDay,
            BackAlleyNight,
            MarketChildDay,
            MarketChildNight,
            MarketAdult,
            TempleOfTimeExteriorChildDay,
            TempleOfTimeExteriorChildNight,
            TempleOfTimeExteriorAdult,
            KnowItAllBrothersHouse,
            HouseOfTwins,
            MidosHouse,
            SariasHouse,
            CarpenterBossHouse,
            BackAlleyManInGreenHouse,
            Bazaar,
            KokiriShop,
            GoronShop,
            ZoraShop,
            KakarikoPotionShop,
            MarketPotionShop,
            BombchuShop,
            HappyMaskShop,
            LinksHouse,
            BackAlleyDogLadyHouse,
            Stable,
            ImpasHouse,
            LakesideLaboratory,
            CarpentersTen,
            GravekeepersHut,
            GreatFairysFountainUpgrades,
            FairysFountain,
            GreatFairysFountainSpells,
            Grottos,
            GraveRedead,
            GraveFairysFountain,
            RoyalFamilyTomb,
            ShootingGallery,
            TempleOfTIme,
            ChamberOfTheSages,
            CastleHedgeMazeDay,
            CastleHedgeMazeNight,
            CutsceneMap,
            Windmill,
            FishingPond,
            CastleCourtyard,
            BombchuBowling,
            RanchHouse,
            GuardHouse,
            GrannysPotionShop,
            GanonBattleArena,
            HouseOfSkulltula,
            HyruleFields,
            KakarikoVillage,
            Graveyard,
            ZorasRiver,
            KokiriForest,
            SacredForestMeadow,
            LakeHylia,
            ZorasDomain,
            ZorasFountain,
            GerudoValley,
            LostWoods,
            DesertColossus,
            GerudosFortress,
            HauntedWasteland,
            HyruleCastle,
            DeathMountainTrail,
            DeathMountainCrater,
            GoronCity,
            LonLonRanch,
            GanonsCastleExterior
        }

        public enum TunicType
        {
            Kokiri,
            Goron,
            Zora,
        }

        public enum SwordType
        {
            None,
            Kokiri,
            Master,
            BigGoron
        }

        public enum ShieldType
        {
            None,
            Deku,
            Hylian,
            Mirror
        }

        public enum BootsType
        {
            Kokiri,
            Iron,
            Hover
        }

        public enum MaskType
        {
            None,
            Keaton,
            Skull,
            Spooky,
            Bunny,
            Goron,
            Zora,
            Gerudo,
            Truth
        }

        public enum MagicType
        {
            None,
            Normal,
            Extended
        }

        public enum ItemType: int
        {
            None = -1,
            DekuStick,
            DekuNut,
            Bomb,
            FairyBow,
            FireArrow,
            DinsFire,
            FairySlingshot,
            FairyOcarina,
            OcarinaOfTime,
            Bombchu,
            Hookshot,
            Longshot,
            IceArrow,
            FaroresWind,
            Boomerang,
            LensOfTruth,
            MagicBean,
            MegatonHammer,
            LightArrow,
            NayrusLove,
            EmptyBottle,
            RedPotion,
            GreenPotion,
            BluePotion,
            BottledFairy,
            BottledFish,
            LonLonMilk,
            RutosLetter,
            BlueFire,
            BottledBugs,
            BottledBigPoe,
            LonLonMilkHalf,
            BottledPoe,
            WeirdEgg,
            ChildCucco,
            ZeldasLetter,
            KeatonMask,
            SkullMask,
            SpookyMask,
            BunnyHood,
            GoronMask,
            ZoraMask,
            GerudoMask,
            MaskOfTruth,
            SoldOut,
            PocketEgg,
            PocketCucco,
            Cojiro,
            OddMushroon,
            OddPotion,
            PoachersSaw,
            BrokenGoronSword,
            Prescription,
            EyeballFrog,
            EyeDrops,
            ClaimCheck,
            BowFireArrows,
            BowIceArrows,
            BowLightArrows
        }

        public enum OcarinaType
        {
            None,
            Fairy,
            Time
        }

        public enum HookshotType
        {
            None,
            Normal,
            Extended
        }

        public enum StrengthType
        {
            None,
            GoronBracelet,
            SilverGauntlets,
            GoldenGauntlets,
            BlackGauntlets,
            GreenGauntlets,
            BlueGauntlets
        }

        public enum WalletType
        {
            Child,
            Adult,
            Giant,
            Tycoon
        }

        public enum ZoraScaleType
        {
            None,
            Silver,
            Golden
        }

        public enum AmmoUpgradeType
        {
            None,
            Base,
            Upgraded,
            Max
        }

        public enum AgeType
        {
            Adult,
            Child
        }
    }
}