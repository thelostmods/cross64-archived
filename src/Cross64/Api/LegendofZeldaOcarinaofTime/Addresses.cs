namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public enum AddressType
        {
            /// <summary> Pointer to the global context </summary>
            Global,

            /// <summary> Address where save data begins </summary>
            Save,

            /// <summary> Where all the graphical data is stored </summary>
            OverlayTable,
            
            /// <summary> Player object </summary>
            Player,

            /// <summary> The interact state the A button image says is available </summary>
            InteractGuiState,
            
            /// <summary> Gui is visible on screen </summary>
            GuiVisible,
            
            /// <summary> Game is paused (Inventory Menu?) </summary>
            Paused
        }

        public enum GameVersion
        {
            DEBUG_1_0,
            PAL_1_0,
            USA_JP_1_0,
            USA_1_1,
            USA_1_2,
        }

        private void SetVersion(GameVersion version)
        {
            Addresses.Clear();

            switch (version)
            {
                case GameVersion.DEBUG_1_0:
                    // Contexts
                    Addresses.Add((int) AddressType.Global, 0x157da0);
                    Addresses.Add((int) AddressType.Save, 0x15e660);
                    
                    // Graphical Data
                    Addresses.Add((int) AddressType.OverlayTable, 0x1162A0);
                    
                    // Player Data
                    Addresses.Add((int) AddressType.Player, 0x2245B0);

                    // Runtime Data
                    //Addresses.Add((int) AddressType.InteractGuiState, 0x000000);
                    Addresses.Add((int) AddressType.GuiVisible, 0x1C4357);
                    Addresses.Add((int) AddressType.Paused, 0x166600);
                    break;
                    
                case GameVersion.USA_JP_1_0:
                    // Contexts
                    Addresses.Add((int) AddressType.Global, 0x11f248);
                    Addresses.Add((int) AddressType.Save, 0x11a5d0);
                    
                    // Graphical Data
                    Addresses.Add((int) AddressType.OverlayTable, 0x0e8530);
                    
                    // Player Data
                    Addresses.Add((int) AddressType.Player, 0x1daa30);

                    // Runtime Data
                    Addresses.Add((int) AddressType.InteractGuiState, 0x1d8b7f);
                    Addresses.Add((int) AddressType.GuiVisible, 0x1d8be3);
                    Addresses.Add((int) AddressType.Paused, 0x1c6fa0);
                    break;
            }
        }
    }
}