using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PokemonStadium2 : ApiBase
    {
        public override string GameName => "Pokémon Stadium	2";
        public override GameID GameID => GameID.PokemonStadium2;

        public PokemonStadium2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
