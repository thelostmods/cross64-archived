using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class EltaleMonsters : ApiBase
    {
        public override string GameName => "Eltale Monsters";
        public override GameID GameID => GameID.EltaleMonsters;

        public EltaleMonsters()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
