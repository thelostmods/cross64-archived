using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ExtremeGXG2 : ApiBase
    {
        public override string GameName => "Extreme-G XG2";
        public override GameID GameID => GameID.ExtremeGXG2;

        public ExtremeGXG2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
