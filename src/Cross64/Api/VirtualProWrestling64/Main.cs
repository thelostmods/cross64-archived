using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class VirtualProWrestling64 : ApiBase
    {
        public override string GameName => "Virtual Pro Wrestling 64";
        public override GameID GameID => GameID.VirtualProWrestling64;

        public VirtualProWrestling64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
