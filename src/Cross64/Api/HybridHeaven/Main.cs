using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HybridHeaven : ApiBase
    {
        public override string GameName => "Hybrid Heaven";
        public override GameID GameID => GameID.HybridHeaven;

        public HybridHeaven()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
