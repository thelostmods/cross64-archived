using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NASCAR2000 : ApiBase
    {
        public override string GameName => "NASCAR 2000";
        public override GameID GameID => GameID.NASCAR2000;

        public NASCAR2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
