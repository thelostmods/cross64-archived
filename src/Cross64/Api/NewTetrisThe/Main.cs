using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NewTetrisThe : ApiBase
    {
        public override string GameName => "New Tetris, The";
        public override GameID GameID => GameID.NewTetrisThe;

        public NewTetrisThe()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
