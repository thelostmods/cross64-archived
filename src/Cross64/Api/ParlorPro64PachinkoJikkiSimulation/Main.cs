using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ParlorPro64PachinkoJikkiSimulation : ApiBase
    {
        public override string GameName => "Parlor! Pro 64: Pachinko Jikki Simulation";
        public override GameID GameID => GameID.ParlorPro64PachinkoJikkiSimulation;

        public ParlorPro64PachinkoJikkiSimulation()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
