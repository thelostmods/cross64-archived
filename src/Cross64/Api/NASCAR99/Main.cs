using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NASCAR99 : ApiBase
    {
        public override string GameName => "NASCAR '99";
        public override GameID GameID => GameID.NASCAR99;

        public NASCAR99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
