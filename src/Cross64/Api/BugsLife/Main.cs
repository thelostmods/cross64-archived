using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BugsLife : ApiBase
    {
        public override string GameName => "A Bug's Life";
        public override GameID GameID => GameID.BugsLife;

        public BugsLife()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
