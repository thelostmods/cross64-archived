using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Bomberman64 : ApiBase
    {
        public override string GameName => "Bomberman 64";
        public override GameID GameID => GameID.Bomberman64;

        public Bomberman64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
