using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TurokJikuSinshi : ApiBase
    {
        public override string GameName => "Turok: Jiku Sinshi";
        public override GameID GameID => GameID.TurokJikuSinshi;

        public TurokJikuSinshi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
