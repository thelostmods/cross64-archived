using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GASPFightersNEXTream : ApiBase
    {
        public override string GameName => "G.A.S.P!! Fighter's NEXTream";
        public override GameID GameID => GameID.GASPFightersNEXTream;

        public GASPFightersNEXTream()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
