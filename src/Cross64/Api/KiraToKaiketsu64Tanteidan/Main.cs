using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KiraToKaiketsu64Tanteidan : ApiBase
    {
        public override string GameName => "Kira To Kaiketsu! 64 Tanteidan";
        public override GameID GameID => GameID.KiraToKaiketsu64Tanteidan;

        public KiraToKaiketsu64Tanteidan()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
