using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MonsterTruckMadness64 : ApiBase
    {
        public override string GameName => "Monster Truck Madness 64";
        public override GameID GameID => GameID.MonsterTruckMadness64;

        public MonsterTruckMadness64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
