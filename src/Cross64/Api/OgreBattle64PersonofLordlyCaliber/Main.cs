using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class OgreBattle64PersonofLordlyCaliber : ApiBase
    {
        public override string GameName => "Ogre Battle 64: Person of Lordly Caliber";
        public override GameID GameID => GameID.OgreBattle64PersonofLordlyCaliber;

        public OgreBattle64PersonofLordlyCaliber()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
