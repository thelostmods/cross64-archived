using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoPowerfulProJakyu6 : ApiBase
    {
        public override string GameName => "Jikkyō Powerful Pro Yakyū 6";
        public override GameID GameID => GameID.JikkyoPowerfulProJakyu6;

        public JikkyoPowerfulProJakyu6()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
