using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CruisnWorld : ApiBase
    {
        public override string GameName => "Cruis'n World";
        public override GameID GameID => GameID.CruisnWorld;

        public CruisnWorld()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
