using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DukeNukem64 : ApiBase
    {
        public override string GameName => "Duke Nukem 64";
        public override GameID GameID => GameID.DukeNukem64;

        public DukeNukem64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
