using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChokukanNightProYakyuKing : ApiBase
    {
        public override string GameName => "Chōkukan Night Pro Yakyu King";
        public override GameID GameID => GameID.ChokukanNightProYakyuKing;

        public ChokukanNightProYakyuKing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
