using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WCWNitro : ApiBase
    {
        public override string GameName => "WCW Nitro";
        public override GameID GameID => GameID.WCWNitro;

        public WCWNitro()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
