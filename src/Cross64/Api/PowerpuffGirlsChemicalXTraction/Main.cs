using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PowerpuffGirlsChemicalXTraction : ApiBase
    {
        public override string GameName => "Powerpuff Girls: Chemical X-Traction";
        public override GameID GameID => GameID.PowerpuffGirlsChemicalXTraction;

        public PowerpuffGirlsChemicalXTraction()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
