using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Mahjong64 : ApiBase
    {
        public override string GameName => "Mahjong 64";
        public override GameID GameID => GameID.Mahjong64;

        public Mahjong64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
