using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarTwins : ApiBase
    {
        public override string GameName => "Star Twins";
        public override GameID GameID => GameID.StarTwins;

        public StarTwins()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
