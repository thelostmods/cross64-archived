using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ScoobyDooClassicCreepCapers : ApiBase
    {
        public override string GameName => "Scooby Doo! Classic Creep Capers";
        public override GameID GameID => GameID.ScoobyDooClassicCreepCapers;

        public ScoobyDooClassicCreepCapers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
