using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Bomberman64ArcadeEdition : ApiBase
    {
        public override string GameName => "Bomberman 64: Arcade Edition";
        public override GameID GameID => GameID.Bomberman64ArcadeEdition;

        public Bomberman64ArcadeEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
