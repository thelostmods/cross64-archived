using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAShowtimeNBAonNBC : ApiBase
    {
        public override string GameName => "NBA Showtime NBA on NBC";
        public override GameID GameID => GameID.NBAShowtimeNBAonNBC;

        public NBAShowtimeNBAonNBC()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
