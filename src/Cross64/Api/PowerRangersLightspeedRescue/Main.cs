using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PowerRangersLightspeedRescue : ApiBase
    {
        public override string GameName => "Power Rangers Lightspeed Rescue";
        public override GameID GameID => GameID.PowerRangersLightspeedRescue;

        public PowerRangersLightspeedRescue()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
