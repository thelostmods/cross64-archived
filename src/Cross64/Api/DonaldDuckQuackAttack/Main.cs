using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DonaldDuckQuackAttack : ApiBase
    {
        public override string GameName => "Donald Duck: Quack Attack";
        public override GameID GameID => GameID.DonaldDuckQuackAttack;

        public DonaldDuckQuackAttack()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
