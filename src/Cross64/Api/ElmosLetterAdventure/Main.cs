using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ElmosLetterAdventure : ApiBase
    {
        public override string GameName => "Elmo's Letter Adventure";
        public override GameID GameID => GameID.ElmosLetterAdventure;

        public ElmosLetterAdventure()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
