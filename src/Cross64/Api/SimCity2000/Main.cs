using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SimCity2000 : ApiBase
    {
        public override string GameName => "SimCity 2000";
        public override GameID GameID => GameID.SimCity2000;

        public SimCity2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
