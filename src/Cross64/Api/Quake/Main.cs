using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Quake : ApiBase
    {
        public override string GameName => "Quake";
        public override GameID GameID => GameID.Quake;

        public Quake()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
