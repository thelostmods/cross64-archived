using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PocketMonstersStadium2 : ApiBase
    {
        public override string GameName => "Pocket Monsters Stadium 2";
        public override GameID GameID => GameID.PocketMonstersStadium2;

        public PocketMonstersStadium2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
