using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BustAMove99 : ApiBase
    {
        public override string GameName => "Bust-A-Move '99";
        public override GameID GameID => GameID.BustAMove99;

        public BustAMove99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
