using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DiddyKongRacing : ApiBase
    {
        public override string GameName => "Diddy Kong Racing";
        public override GameID GameID => GameID.DiddyKongRacing;

        public DiddyKongRacing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
