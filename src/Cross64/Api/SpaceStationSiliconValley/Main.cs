using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SpaceStationSiliconValley : ApiBase
    {
        public override string GameName => "Space Station Silicon Valley";
        public override GameID GameID => GameID.SpaceStationSiliconValley;

        public SpaceStationSiliconValley()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
