using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DyuaruhirozuDaizenshuDualHeroes : ApiBase
    {
        public override string GameName => "Dyuaruhirozu Daizenshu: Dual Heroes";
        public override GameID GameID => GameID.DyuaruhirozuDaizenshuDualHeroes;

        public DyuaruhirozuDaizenshuDualHeroes()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
