using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PaperMario : ApiBase
    {
        public override string GameName => "Paper Mario";
        public override GameID GameID => GameID.PaperMario;

        public PaperMario()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
