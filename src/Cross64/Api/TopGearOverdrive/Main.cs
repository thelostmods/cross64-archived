using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TopGearOverdrive : ApiBase
    {
        public override string GameName => "Top Gear Overdrive";
        public override GameID GameID => GameID.TopGearOverdrive;

        public TopGearOverdrive()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
