using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BassHunter64 : ApiBase
    {
        public override string GameName => "Bass Hunter 64";
        public override GameID GameID => GameID.BassHunter64;

        public BassHunter64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
