using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAJam2000 : ApiBase
    {
        public override string GameName => "NBA Jam 2000";
        public override GameID GameID => GameID.NBAJam2000;

        public NBAJam2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
