using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RugratsTreasureHunt : ApiBase
    {
        public override string GameName => "Rugrats: Treasure Hunt";
        public override GameID GameID => GameID.RugratsTreasureHunt;

        public RugratsTreasureHunt()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
