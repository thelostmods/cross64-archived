using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AkumajoDraculaMokushiHashumi : ApiBase
    {
        public override string GameName => "Akumajo Dracula Mokushi Hashumi";
        public override GameID GameID => GameID.AkumajoDraculaMokushiHashumi;

        public AkumajoDraculaMokushiHashumi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
