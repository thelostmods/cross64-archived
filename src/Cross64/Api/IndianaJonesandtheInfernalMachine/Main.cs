using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class IndianaJonesandtheInfernalMachine : ApiBase
    {
        public override string GameName => "Indiana Jones and the Infernal Machine";
        public override GameID GameID => GameID.IndianaJonesandtheInfernalMachine;

        public IndianaJonesandtheInfernalMachine()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
