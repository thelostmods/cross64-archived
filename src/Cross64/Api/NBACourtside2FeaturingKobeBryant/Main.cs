using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBACourtside2FeaturingKobeBryant : ApiBase
    {
        public override string GameName => "NBA Courtside 2: Featuring Kobe Bryant";
        public override GameID GameID => GameID.NBACourtside2FeaturingKobeBryant;

        public NBACourtside2FeaturingKobeBryant()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
