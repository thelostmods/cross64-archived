using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NushiTsuri64ShiokazeNiNotte : ApiBase
    {
        public override string GameName => "Nushi Tsuri 64: Shiokaze Ni Notte";
        public override GameID GameID => GameID.NushiTsuri64ShiokazeNiNotte;

        public NushiTsuri64ShiokazeNiNotte()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
