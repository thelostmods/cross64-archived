using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MilosAstroLanes : ApiBase
    {
        public override string GameName => "Milo's Astro Lanes";
        public override GameID GameID => GameID.MilosAstroLanes;

        public MilosAstroLanes()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
