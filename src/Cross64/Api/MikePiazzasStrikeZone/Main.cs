using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MikePiazzasStrikeZone : ApiBase
    {
        public override string GameName => "Mike Piazza's Strike Zone";
        public override GameID GameID => GameID.MikePiazzasStrikeZone;

        public MikePiazzasStrikeZone()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
