using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WayneGretzkys3DHockey98 : ApiBase
    {
        public override string GameName => "Wayne Gretzky's 3D Hockey 98";
        public override GameID GameID => GameID.WayneGretzkys3DHockey98;

        public WayneGretzkys3DHockey98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
