using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Doom64 : ApiBase
    {
        public override string GameName => "Doom 64";
        public override GameID GameID => GameID.Doom64;

        public Doom64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
