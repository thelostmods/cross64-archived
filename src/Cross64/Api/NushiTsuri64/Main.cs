using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NushiTsuri64 : ApiBase
    {
        public override string GameName => "Nushi Tsuri 64";
        public override GameID GameID => GameID.NushiTsuri64;

        public NushiTsuri64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
