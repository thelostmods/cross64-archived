using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LooneyTunesDaffyDuckStarringasDuckDodgers : ApiBase
    {
        public override string GameName => "Looney Tunes Daffy Duck Starring as Duck Dodgers";
        public override GameID GameID => GameID.LooneyTunesDaffyDuckStarringasDuckDodgers;

        public LooneyTunesDaffyDuckStarringasDuckDodgers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
