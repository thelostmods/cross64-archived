using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperBowling : ApiBase
    {
        public override string GameName => "Super Bowling";
        public override GameID GameID => GameID.SuperBowling;

        public SuperBowling()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
