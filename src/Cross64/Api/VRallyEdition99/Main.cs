using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class VRallyEdition99 : ApiBase
    {
        public override string GameName => "V-Rally Edition '99";
        public override GameID GameID => GameID.VRallyEdition99;

        public VRallyEdition99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
