using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PGAEuropeanTour : ApiBase
    {
        public override string GameName => "PGA European Tour";
        public override GameID GameID => GameID.PGAEuropeanTour;

        public PGAEuropeanTour()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
