using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MickeyNoRacingChallengeUSA : ApiBase
    {
        public override string GameName => "Mickey No Racing Challenge USA";
        public override GameID GameID => GameID.MickeyNoRacingChallengeUSA;

        public MickeyNoRacingChallengeUSA()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
