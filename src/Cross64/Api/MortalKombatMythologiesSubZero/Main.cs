using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MortalKombatMythologiesSubZero : ApiBase
    {
        public override string GameName => "Mortal Kombat Mythologies: Sub-Zero";
        public override GameID GameID => GameID.MortalKombatMythologiesSubZero;

        public MortalKombatMythologiesSubZero()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
