using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NeonGenesisEvangelion : ApiBase
    {
        public override string GameName => "Neon Genesis Evangelion";
        public override GameID GameID => GameID.NeonGenesisEvangelion;

        public NeonGenesisEvangelion()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
