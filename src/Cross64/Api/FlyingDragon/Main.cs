using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FlyingDragon : ApiBase
    {
        public override string GameName => "Flying Dragon";
        public override GameID GameID => GameID.FlyingDragon;

        public FlyingDragon()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
