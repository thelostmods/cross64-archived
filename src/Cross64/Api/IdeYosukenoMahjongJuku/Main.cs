using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class IdeYosukenoMahjongJuku : ApiBase
    {
        public override string GameName => "Ide Yōsuke no Mahjong Juku";
        public override GameID GameID => GameID.IdeYosukenoMahjongJuku;

        public IdeYosukenoMahjongJuku()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
