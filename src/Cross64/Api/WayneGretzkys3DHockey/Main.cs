using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WayneGretzkys3DHockey : ApiBase
    {
        public override string GameName => "Wayne Gretzky's 3D Hockey";
        public override GameID GameID => GameID.WayneGretzkys3DHockey;

        public WayneGretzkys3DHockey()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
