using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SpaceDynamites : ApiBase
    {
        public override string GameName => "Space Dynamites";
        public override GameID GameID => GameID.SpaceDynamites;

        public SpaceDynamites()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
