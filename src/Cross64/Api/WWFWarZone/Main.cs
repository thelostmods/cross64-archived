using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WWFWarZone : ApiBase
    {
        public override string GameName => "WWF War Zone";
        public override GameID GameID => GameID.WWFWarZone;

        public WWFWarZone()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
