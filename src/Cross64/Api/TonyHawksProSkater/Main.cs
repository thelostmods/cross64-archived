using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TonyHawksProSkater : ApiBase
    {
        public override string GameName => "Tony Hawk's Pro Skater";
        public override GameID GameID => GameID.TonyHawksProSkater;

        public TonyHawksProSkater()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
