using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HeiwaPachinkWorld64 : ApiBase
    {
        public override string GameName => "Heiwa Pachinko World 64";
        public override GameID GameID => GameID.HeiwaPachinkWorld64;

        public HeiwaPachinkWorld64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
