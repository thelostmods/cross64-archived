using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Yakouchuu2SatsujinKouru : ApiBase
    {
        public override string GameName => "Yakouchuu 2: Satsujin Kouru";
        public override GameID GameID => GameID.Yakouchuu2SatsujinKouru;

        public Yakouchuu2SatsujinKouru()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
