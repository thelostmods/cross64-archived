using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PennyRacers : ApiBase
    {
        public override string GameName => "Penny Racers";
        public override GameID GameID => GameID.PennyRacers;

        public PennyRacers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
