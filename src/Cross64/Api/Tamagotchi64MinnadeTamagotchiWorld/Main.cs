using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Tamagotchi64MinnadeTamagotchiWorld : ApiBase
    {
        public override string GameName => "Tamagotchi 64: Minna de Tamagotchi World";
        public override GameID GameID => GameID.Tamagotchi64MinnadeTamagotchiWorld;

        public Tamagotchi64MinnadeTamagotchiWorld()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
