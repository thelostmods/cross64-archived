using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SnobowKids : ApiBase
    {
        public override string GameName => "Snobow Kids";
        public override GameID GameID => GameID.SnobowKids;

        public SnobowKids()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
