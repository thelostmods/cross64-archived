using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Supercross2000 : ApiBase
    {
        public override string GameName => "Supercross 2000";
        public override GameID GameID => GameID.Supercross2000;

        public Supercross2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
