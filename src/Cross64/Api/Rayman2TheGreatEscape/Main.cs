using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Rayman2TheGreatEscape : ApiBase
    {
        public override string GameName => "Rayman 2: The Great Escape";
        public override GameID GameID => GameID.Rayman2TheGreatEscape;

        public Rayman2TheGreatEscape()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
