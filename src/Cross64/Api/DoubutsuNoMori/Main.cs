using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DoubutsuNoMori : ApiBase
    {
        public override string GameName => "Doubutsu No Mori";
        public override GameID GameID => GameID.DoubutsuNoMori;

        public DoubutsuNoMori()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
