using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Starcraft64 : ApiBase
    {
        public override string GameName => "Starcraft 64";
        public override GameID GameID => GameID.Starcraft64;

        public Starcraft64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
