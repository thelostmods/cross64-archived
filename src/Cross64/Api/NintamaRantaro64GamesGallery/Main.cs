using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NintamaRantaro64GamesGallery : ApiBase
    {
        public override string GameName => "Nintama Rantaro 64: Games Gallery";
        public override GameID GameID => GameID.NintamaRantaro64GamesGallery;

        public NintamaRantaro64GamesGallery()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
