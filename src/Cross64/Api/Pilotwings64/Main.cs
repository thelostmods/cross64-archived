using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Pilotwings64 : ApiBase
    {
        public override string GameName => "Pilotwings 64";
        public override GameID GameID => GameID.Pilotwings64;

        public Pilotwings64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
