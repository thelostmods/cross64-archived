using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WWFNoMercy : ApiBase
    {
        public override string GameName => "WWF No Mercy";
        public override GameID GameID => GameID.WWFNoMercy;

        public WWFNoMercy()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
