using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioSutori : ApiBase
    {
        public override string GameName => "Mario Sutōrī";
        public override GameID GameID => GameID.MarioSutori;

        public MarioSutori()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
