using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Hexen : ApiBase
    {
        public override string GameName => "Hexen";
        public override GameID GameID => GameID.Hexen;

        public Hexen()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
