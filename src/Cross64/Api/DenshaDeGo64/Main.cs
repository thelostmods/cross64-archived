using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DenshaDeGo64 : ApiBase
    {
        public override string GameName => "Densha De Go!! 64";
        public override GameID GameID => GameID.DenshaDeGo64;

        public DenshaDeGo64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
