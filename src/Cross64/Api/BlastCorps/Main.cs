using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BlastCorps : ApiBase
    {
        public override string GameName => "Blast Corps";
        public override GameID GameID => GameID.BlastCorps;

        public BlastCorps()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
