using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ECWHardcoreRevolution : ApiBase
    {
        public override string GameName => "ECW Hardcore Revolution";
        public override GameID GameID => GameID.ECWHardcoreRevolution;

        public ECWHardcoreRevolution()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
