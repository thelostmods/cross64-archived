using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NHLPro99 : ApiBase
    {
        public override string GameName => "NHL Pro '99";
        public override GameID GameID => GameID.NHLPro99;

        public NHLPro99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
