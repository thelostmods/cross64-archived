﻿using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoTooie : ApiBase
    {
        public override string GameName => "Banjo-Tooie";
        public override GameID GameID => GameID.BanjoTooie;

        public BanjoTooie()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}