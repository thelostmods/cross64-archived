using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TopGearRally : ApiBase
    {
        public override string GameName => "Top Gear Rally";
        public override GameID GameID => GameID.TopGearRally;

        public TopGearRally()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
