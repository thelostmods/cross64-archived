using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BattlezoneRiseOfTheBlackDogs : ApiBase
    {
        public override string GameName => "Battlezone: Rise of the Black Dogs";
        public override GameID GameID => GameID.BattlezoneRiseOfTheBlackDogs;

        public BattlezoneRiseOfTheBlackDogs()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
