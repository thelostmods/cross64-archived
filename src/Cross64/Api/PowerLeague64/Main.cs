using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PowerLeague64 : ApiBase
    {
        public override string GameName => "Power League 64";
        public override GameID GameID => GameID.PowerLeague64;

        public PowerLeague64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
