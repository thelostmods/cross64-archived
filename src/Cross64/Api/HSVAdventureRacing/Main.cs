using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HSVAdventureRacing : ApiBase
    {
        public override string GameName => "HSV Adventure Racing";
        public override GameID GameID => GameID.HSVAdventureRacing;

        public HSVAdventureRacing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
