using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CommandAndConquer : ApiBase
    {
        public override string GameName => "Command & Conquer";
        public override GameID GameID => GameID.CommandAndConquer;

        public CommandAndConquer()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
