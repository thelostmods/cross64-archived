﻿using System;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie : ApiBase
    {
        public CameraDef Camera { get; private set; }
        public PlayerDef Player { get; private set; }
        public RuntimeDef Runtime { get; private set; }
        public SaveDef Save { get; private set; }

        public override string GameName => "Banjo-Kazooie";
        public override GameID GameID => GameID.BanjoKazooie;
        public GameVersion Version { get; private set; }

        public bool IsPlaying => !(Player.MovementState == 0 ||
                                   Runtime.CurrentProfile == ProfileType.Title);
        
        internal override void Initialize()
        {
            Version = (Emulator.Cartridge.Country + Emulator.Cartridge.Revision) switch
            {
                "J0" => GameVersion.JP_1_0,
                "P0" => GameVersion.PAL_1_0,
                "E0" => GameVersion.USA_1_0,
                "E1" => GameVersion.USA_1_1,
                _ => throw new Exception("[Rom corruption]: No country code exists")
            };
            SetVersion(Version);

            Camera = new CameraDef();
            Player = new PlayerDef();
            Runtime = new RuntimeDef();
            Save = new SaveDef();
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
            if (Player == null || !Player.Exists) return;
            Camera.OnTick();
            Player.OnTick();
            
            DetectMap();
        }
    }
}