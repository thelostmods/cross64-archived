﻿using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie : ApiBase
    {
        public delegate void SceneChangeEvent(SceneType scn, LevelType lvl);

        public event SceneChangeEvent OnSceneChange;

        private uint curScn;

        private void DetectMap()
        {
            var scn = (uint) Runtime.CurrentScene;
            if (scn == 0 || scn == curScn) return;
            curScn = scn;

            var ptr = Addresses[(int) AddressType.RtCurLevelLookup];
            var val = Emulator.RdRam.Read16(ptr);
            while (val != scn)
            {
                ptr += 0x8;
                val = Emulator.RdRam.Read16(ptr);
            }

            var lvl = val;
            if (lvl > (ushort) LevelType.TitleScreen)
                Runtime.CurrentLevel = LevelType.RomhackLevel;
            else Runtime.CurrentLevel = (LevelType) lvl;

            OnSceneChange?.Invoke((SceneType) scn, (LevelType) lvl);
        }
    }
}