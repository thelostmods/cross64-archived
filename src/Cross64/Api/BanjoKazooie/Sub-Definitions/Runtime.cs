﻿using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class RuntimeDef
        {
            public readonly BufferObject CurrentEventsLevel = new(Addresses[(int) AddressType.RtCurLevelEvents], 0x4);
            public readonly BufferObject CurrentEventsScene = new(Addresses[(int) AddressType.RtCurSceneEvents], 0x4);

            private Emulator.Pointer addrCurExit = Addresses[(int) AddressType.RtCurExit];

            private Emulator.Pointer addrCurHealth =
                Addresses[(int) AddressType.Inventory] + (uint) InventoryType.Health;

            private Emulator.Pointer addrCurLevel = Addresses[(int) AddressType.RtCurLevel];
            private Emulator.Pointer addrCurProfile = Addresses[(int) AddressType.RtCurProfile];
            private Emulator.Pointer addrCurScene = Addresses[(int) AddressType.RtCurScene];
            private Emulator.Pointer addrIsCutscene = Addresses[(int) AddressType.RtIsCutscene];
            private Emulator.Pointer addrIsLoading = Addresses[(int) AddressType.RtIsLoading];
            private Emulator.Pointer addrTransitionState = Addresses[(int) AddressType.RtTransitionState];
            private int curLevel = (int) LevelType.Unknown;

            public ExitType CurrentExit
            {
                get
                {
                    var exit = addrCurExit.U8;
                    if ((int) ExitType.Unknown < exit ||
                        exit < (int) ExitType.SmMainGruntildasLair)
                        return ExitType.Unknown;
                    return (ExitType) exit;
                }
                set => addrCurExit.U8 = (byte) value;
            }

            public int Health
            {
                get => (int) addrCurHealth.U32;
                set => addrCurHealth.U32 = value < 0 ? 0 : (uint) value;
            }

            public LevelType CurrentLevel
            {
                get
                {
                    return curLevel switch
                    {
                        < (int) LevelType.MumbosMountain => LevelType.Unknown,
                        < (int) LevelType.TitleScreen => LevelType.RomhackLevel,
                        _ => (LevelType) curLevel
                    };
                }
                set => curLevel = (int) value;
            }

            public SceneType CurrentScene
            {
                get
                {
                    var scene = addrCurScene.U8;
                    return scene switch
                    {
                        < (byte) SceneType.SmMain => SceneType.Unknown,
                        < (byte) SceneType.IntroGruntyThreat2 => SceneType.RomhackScene,
                        _ => (SceneType) scene
                    };
                }
                set => addrCurScene.U8 = (byte) value;
            }

            public ProfileType CurrentProfile
            {
                get => (ProfileType) addrCurProfile.U32;
                set => addrCurProfile.U32 = (uint) value;
            }

            public bool IsCutscene => addrIsCutscene.U32 != 0;

            public bool IsLoading => addrIsLoading.U8 != 0;

            public TransitionType TransitionState => (TransitionType) addrTransitionState.U8;

            public void GotoScene(ushort value)
            {
                addrCurScene.U8 = (byte) (value >> 8);
                addrCurExit.U8 = (byte) value;
                addrIsLoading.U8 = 1;
            }

            public void GotoScene(byte scene, byte exit)
            {
                addrCurScene.U8 = scene;
                addrCurExit.U8 = exit;
                addrIsLoading.U8 = 1;
            }

            public void GotoScene(SceneType scene, ExitType exit)
            {
                addrCurScene.U8 = (byte) scene;
                addrCurExit.U8 = (byte) exit;
                addrIsLoading.U8 = 1;
            }
        }
    }
}