﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class CameraDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Camera];

            private Utility.Vector3 oldPos = new();
            private Utility.Vector3 newPos = new();
            private Utility.Vector3 oldVel = new();
            private Utility.Vector3 newVel = new();

            #region Position

            public Utility.Vector3 PrevPosition => new(oldPos);
            public float PrevPosX => oldPos.X;
            public float PrevPosY => oldPos.Y;
            public float PrevPosZ => oldPos.Z;

            public Utility.Vector3 Position
            {
                get => new(newPos);
                set
                {
                    newPos = value;
                    _instance.WriteB(value.ToArray());
                }
            }

            public float PosX
            {
                get => newPos.X;
                set
                {
                    newPos.X = value;
                    _instance.WriteF(value);
                }
            }

            public float PosY
            {
                get => newPos.Y;
                set
                {
                    newPos.Y = value;
                    _instance.WriteF(value, 0x4);
                }
            }

            public float PosZ
            {
                get => newPos.Z;
                set
                {
                    newPos.Z = value;
                    _instance.WriteF(value, 0x8);
                }
            }

            #endregion

            #region Rotation

            public Utility.Vector3 Rotation
            {
                get => new(RotX, RotY, RotZ);
                set
                {
                    RotX = value.X;
                    RotY = value.Y;
                    RotZ = value.Z;
                }
            }

            public float RotX
            {
                get => _instance.ReadF(0x10);
                set => _instance.WriteF(value, 0x10);
            }

            public float RotY
            {
                get => _instance.ReadF(0x14);
                set => _instance.WriteF(value, 0x14);
            }

            public float RotZ
            {
                get => _instance.ReadF(0xc);
                set => _instance.WriteF(value, 0xc);
            }

            #endregion

            internal void OnTick()
            {
                oldPos = newPos;

                newPos = new Utility.Vector3(_instance.ReadB(12));
            }
        }
    }
}