﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class InventoryDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Inventory];

            public int Acorns
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlAcorns);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlAcorns);
            }

            public int Caterpillars
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlCaterpillars);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlCaterpillars);
            }

            public int Eggs
            {
                get => (int) _instance.Read32((int) InventoryType.Eggs);
                set
                {
                    value = value switch {< 0 => 0, > 200 => 200, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.Eggs);
                }
            }

            public int GoldBullions
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlGoldBullion);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlGoldBullion);
            }

            public int GoldFeathers
            {
                get => (int) _instance.Read32((int) InventoryType.FeathersGold);
                set
                {
                    value = value switch {< 0 => 0, > 20 => 20, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.FeathersGold);
                }
            }

            public int HealthUpgrades
            {
                get => (int) _instance.Read32((int) InventoryType.HealthContainers) - 5;
                set
                {
                    value = value switch {< 0 => 0, > 4 => 4, _ => value};
                    _instance.Write32((uint) value + 5, (int) InventoryType.HealthContainers);
                }
            }

            public int HoneyCombs
            {
                get => (int) _instance.Read32((int) InventoryType.HoneycombsEmpty);
                set
                {
                    value = value switch {< 0 => 0, > 6 => 6, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.HoneycombsEmpty);
                }
            }

            public int Jiggies
            {
                get => (int) _instance.Read32((int) InventoryType.Jiggies);
                set
                {
                    if (value < 0) value = 0;
                    _instance.Write32((uint) value, (int) InventoryType.Jiggies);
                    _instance.Write32((uint) value, (int) InventoryType.TextJiggies);
                }
            }

            public int Jinjos
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlJinjos);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlJinjos);
            }

            public int Lives
            {
                get => (int) _instance.Read32((int) InventoryType.Lives);
                set
                {
                    value = value switch {< 0 => 0, > 9 => 9, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.Lives);
                }
            }

            public int Notes
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlNotes);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlNotes);
            }

            public int Oranges
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlOrange);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlOrange);
            }

            public int PresentGreen
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlPresentGreen);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlPresentGreen);
            }

            public int PresentBlue
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlPresentBlue);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlPresentBlue);
            }

            public int PresentRed
            {
                get => (int) _instance.Read32((int) InventoryType.CurLvlPresentRed);
                set => _instance.Write32((uint) value, (int) InventoryType.CurLvlPresentRed);
            }

            public int RedFeathers
            {
                get => (int) _instance.Read32((int) InventoryType.FeathersRed);
                set
                {
                    value = value switch {< 0 => 0, > 100 => 100, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.FeathersRed);
                }
            }

            public int Tokens
            {
                get => (int) _instance.Read32((int) InventoryType.MumboTokensHeld);
                set
                {
                    value = value switch {< 0 => 0, > 99 => 99, _ => value};
                    _instance.Write32((uint) value, (int) InventoryType.MumboTokensHeld);
                }
            }
        }
    }
}