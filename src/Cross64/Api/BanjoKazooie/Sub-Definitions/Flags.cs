﻿using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class FlagsDef
        {
            public readonly BufferObject Cheat = new(Addresses[(int) AddressType.SaveCheatFlags], 0x19);
            public readonly BufferObject Game = new(Addresses[(int) AddressType.SaveGameFlags], 0x20);
            public readonly BufferObject HoneyComb = new(Addresses[(int) AddressType.SaveHoneycombFlags], 0x3);
            public readonly BufferObject Jiggy = new(Addresses[(int) AddressType.SaveJiggyFlags], 0xd);
            public readonly BufferObject Token = new(Addresses[(int) AddressType.SaveMumboTokenFlags], 0x10);
        }
    }
}