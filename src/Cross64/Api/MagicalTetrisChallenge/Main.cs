using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MagicalTetrisChallenge : ApiBase
    {
        public override string GameName => "Magical Tetris Challenge";
        public override GameID GameID => GameID.MagicalTetrisChallenge;

        public MagicalTetrisChallenge()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
