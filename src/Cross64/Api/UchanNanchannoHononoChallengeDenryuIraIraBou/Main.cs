using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class UchanNanchannoHononoChallengeDenryuIraIraBou : ApiBase
    {
        public override string GameName => "Uchan Nanchan no Hono no Challenge: Denryu Ira Ira Bou";
        public override GameID GameID => GameID.UchanNanchannoHononoChallengeDenryuIraIraBou;

        public UchanNanchannoHononoChallengeDenryuIraIraBou()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
