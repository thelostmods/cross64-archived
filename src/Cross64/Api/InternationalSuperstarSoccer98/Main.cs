using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class InternationalSuperstarSoccer98 : ApiBase
    {
        public override string GameName => "International Superstar Soccer '98";
        public override GameID GameID => GameID.InternationalSuperstarSoccer98;

        public InternationalSuperstarSoccer98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
