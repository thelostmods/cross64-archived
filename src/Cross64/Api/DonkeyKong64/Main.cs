﻿using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DonkeyKong64 : ApiBase
    {
        public override string GameName => "Donkey Kong 64";
        public override GameID GameID => GameID.DonkeyKong64;

        public DonkeyKong64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}