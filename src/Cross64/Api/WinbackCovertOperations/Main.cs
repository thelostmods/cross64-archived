using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WinbackCovertOperations : ApiBase
    {
        public override string GameName => "Winback: Covert Operations";
        public override GameID GameID => GameID.WinbackCovertOperations;

        public WinbackCovertOperations()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
