using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RoadRash64 : ApiBase
    {
        public override string GameName => "Road Rash 64";
        public override GameID GameID => GameID.RoadRash64;

        public RoadRash64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
