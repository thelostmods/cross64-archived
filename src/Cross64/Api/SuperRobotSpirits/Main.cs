using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperRobotSpirits : ApiBase
    {
        public override string GameName => "Super Robot Spirits";
        public override GameID GameID => GameID.SuperRobotSpirits;

        public SuperRobotSpirits()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
