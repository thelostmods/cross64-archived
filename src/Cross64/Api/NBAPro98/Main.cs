using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAPro98 : ApiBase
    {
        public override string GameName => "NBA Pro '98";
        public override GameID GameID => GameID.NBAPro98;

        public NBAPro98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
