using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WorldCup98 : ApiBase
    {
        public override string GameName => "World Cup '98";
        public override GameID GameID => GameID.WorldCup98;

        public WorldCup98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
