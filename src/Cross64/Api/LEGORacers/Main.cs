using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LEGORacers : ApiBase
    {
        public override string GameName => "LEGO Racers";
        public override GameID GameID => GameID.LEGORacers;

        public LEGORacers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
