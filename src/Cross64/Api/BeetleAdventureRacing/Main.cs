using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BeetleAdventureRacing : ApiBase
    {
        public override string GameName => "Beetle Adventure Racing!";
        public override GameID GameID => GameID.BeetleAdventureRacing;

        public BeetleAdventureRacing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
