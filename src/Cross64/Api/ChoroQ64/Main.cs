using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChoroQ64 : ApiBase
    {
        public override string GameName => "Choro Q 64";
        public override GameID GameID => GameID.ChoroQ64;

        public ChoroQ64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
