using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HanafudaTenshiNoYakusoku : ApiBase
    {
        public override string GameName => "007: Hanafuda: Tenshi no Yakusoku";
        public override GameID GameID => GameID.HanafudaTenshiNoYakusoku;

        public HanafudaTenshiNoYakusoku()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
