using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ViolenceKillerTurokNewGeneration : ApiBase
    {
        public override string GameName => "Violence Killer: Turok New Generation";
        public override GameID GameID => GameID.ViolenceKillerTurokNewGeneration;

        public ViolenceKillerTurokNewGeneration()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
