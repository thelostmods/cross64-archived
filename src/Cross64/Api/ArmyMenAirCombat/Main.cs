using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ArmyMenAirCombat : ApiBase
    {
        public override string GameName => "Army Men: Air Combat";
        public override GameID GameID => GameID.ArmyMenAirCombat;

        public ArmyMenAirCombat()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
