using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAInTheZone2 : ApiBase
    {
        public override string GameName => "NBA In The Zone 2";
        public override GameID GameID => GameID.NBAInTheZone2;

        public NBAInTheZone2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
