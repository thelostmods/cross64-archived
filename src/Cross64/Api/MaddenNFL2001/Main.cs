using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaddenNFL2001 : ApiBase
    {
        public override string GameName => "Madden NFL 2001";
        public override GameID GameID => GameID.MaddenNFL2001;

        public MaddenNFL2001()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
