using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MonacoGrandPrix : ApiBase
    {
        public override string GameName => "Monaco Grand Prix";
        public override GameID GameID => GameID.MonacoGrandPrix;

        public MonacoGrandPrix()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
