using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MischiefMakers : ApiBase
    {
        public override string GameName => "Mischief Makers";
        public override GameID GameID => GameID.MischiefMakers;

        public MischiefMakers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
