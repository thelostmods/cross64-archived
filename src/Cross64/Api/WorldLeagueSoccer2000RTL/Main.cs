using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WorldLeagueSoccer2000RTL : ApiBase
    {
        public override string GameName => "World League Soccer 2000, RTL";
        public override GameID GameID => GameID.WorldLeagueSoccer2000RTL;

        public WorldLeagueSoccer2000RTL()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
