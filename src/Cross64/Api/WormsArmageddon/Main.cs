using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WormsArmageddon : ApiBase
    {
        public override string GameName => "Worms Armageddon";
        public override GameID GameID => GameID.WormsArmageddon;

        public WormsArmageddon()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
