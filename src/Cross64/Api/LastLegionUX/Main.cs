using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LastLegionUX : ApiBase
    {
        public override string GameName => "Last Legion UX";
        public override GameID GameID => GameID.LastLegionUX;

        public LastLegionUX()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
