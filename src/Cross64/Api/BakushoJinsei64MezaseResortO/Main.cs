using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BakushoJinsei64MezaseResortO : ApiBase
    {
        public override string GameName => "Bakushō Jinsei 64: Mezase! Resort Ō";
        public override GameID GameID => GameID.BakushoJinsei64MezaseResortO;

        public BakushoJinsei64MezaseResortO()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
