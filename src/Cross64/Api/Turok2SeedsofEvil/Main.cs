using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Turok2SeedsofEvil : ApiBase
    {
        public override string GameName => "Turok 2: Seeds of Evil";
        public override GameID GameID => GameID.Turok2SeedsofEvil;

        public Turok2SeedsofEvil()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
