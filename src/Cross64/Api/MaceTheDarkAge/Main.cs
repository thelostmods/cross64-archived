using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaceTheDarkAge : ApiBase
    {
        public override string GameName => "Mace - The Dark Age";
        public override GameID GameID => GameID.MaceTheDarkAge;

        public MaceTheDarkAge()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
