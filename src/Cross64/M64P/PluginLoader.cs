﻿using System;
using System.Runtime.InteropServices;

namespace Cross64
{
    internal static partial class M64P
    {
        internal static class PluginLoader
        {
            public static readonly string[] VideoPlugin =
            {
                "mupen64plus-video-gliden64",
                "mupen64plus-video-angrylion-plus"
            };

            private static int curVideo = 0;

            internal static IntPtr AudioPtr;
            internal static IntPtr CorePtr;
            internal static IntPtr InputPtr;
            internal static IntPtr RspPtr;
            internal static IntPtr VideoPtr;

            private static string GetLib(string name)
            {
                var path = $"{Environment.CurrentDirectory}/emulator/";

#if WINDOWS
                var env = Environment.GetEnvironmentVariable("Path");
                if (env != null && !env.Contains(path))
                    Environment.SetEnvironmentVariable("Path", $"{env};{path};");

                return $"{path}{name}.dll";
#elif LINUX
                return $"{path}{name}.so";
#elif OSX
                return $"{path}{name}.dylib";
#endif
            }

            private static T BindFunc<T>(string funcName)
            {
                if (!NativeLibrary.TryGetExport(CorePtr, funcName, out IntPtr func))
                    throw new Exception($"BindFuc could not load function[{funcName}] from native!");
                return Marshal.GetDelegateForFunctionPointer<T>(func);
            }

            internal static void Initialize()
            {
                if (CorePtr != default) return;

                if (!NativeLibrary.TryLoad(GetLib("mupen64plus"), out CorePtr))
                    throw new Exception("Mupen64Plus-Core could not be loaded!");

                // Config
                ConfigOpenSection = BindFunc<_ConfigOpenSection>("ConfigOpenSection");
                ConfigDeleteSection = BindFunc<_ConfigDeleteSection>("ConfigDeleteSection");
                ConfigSaveSection = BindFunc<_ConfigSaveSection>("ConfigSaveSection");
                ConfigSaveFile = BindFunc<_ConfigSaveFile>("ConfigSaveFile");
                ConfigSetDefaultFloat = BindFunc<_ConfigSetDefaultFloat>("ConfigSetDefaultFloat");
                ConfigSetDefaultInt = BindFunc<_ConfigSetDefaultInt>("ConfigSetDefaultInt");
                ConfigSetDefaultString = BindFunc<_ConfigSetDefaultString>("ConfigSetDefaultString");
                ConfigGetParamFloat = BindFunc<_ConfigGetParamFloat>("ConfigGetParamFloat");
                ConfigGetParamInt = BindFunc<_ConfigGetParamInt>("ConfigGetParamInt");
                ConfigGetParamString = BindFunc<_ConfigGetParamString>("ConfigGetParamString");
                ConfigSetParameterFloat = BindFunc<_ConfigSetParameterFloat>("ConfigSetParameter");
                ConfigSetParameterInt = BindFunc<_ConfigSetParameterInt>("ConfigSetParameter");
                ConfigSetParameterString = BindFunc<_ConfigSetParameterString>("ConfigSetParameter");
                // ConfigListSections = BindFunc<_ConfigListSections>("ConfigListSections");
                // ConfigListParameters = BindFunc<_ConfigListParameters>("ConfigListParameters");
                // ConfigGetParameterType = BindFunc<_ConfigGetParameterType>("ConfigGetParameterType");
                // ConfigGetParameterHelp = BindFunc<_ConfigGetParameterHelp>("ConfigGetParameterHelp");

                // Core
                CoreStartup = BindFunc<_CoreStartup>("CoreStartup");
                CoreShutdown = BindFunc<_CoreShutdown>("CoreShutdown");
                CoreAttachPlugin = BindFunc<_CoreAttachPlugin>("CoreAttachPlugin");
                CoreDetachPlugin = BindFunc<_CoreDetachPlugin>("CoreDetachPlugin");
                CoreDoCommand = BindFunc<_CoreDoCommand>("CoreDoCommand");
                CoreDoCommandBytes = BindFunc<_CoreDoCommandBytes>("CoreDoCommand");
                CoreDoCommandString = BindFunc<_CoreDoCommandString>("CoreDoCommand");
                CoreDoCommandInt = BindFunc<_CoreDoCommandInt>("CoreDoCommand");
                CoreDoCommandCallback = BindFunc<_CoreDoCommandCallback>("CoreDoCommand");
                CoreOverrideVidExt = BindFunc<_CoreOverrideVidExt>("CoreOverrideVidExt");
                //CoreAddCheat = BindFunc<_CoreAddCheat>("CoreAddCheat");
                //CoreCheatEnabled = BindFunc<_CoreCheatEnabled>("CoreCheatEnabled");
                CoreSaveOverride = BindFunc<_CoreSaveOverride>("CoreSaveOverride");

                // Debug

                // Memory
                GetRdRam = BindFunc<_GetRdRam>("GetRdRam");
                GetRom = BindFunc<_GetRom>("GetRom");
                GetRdRamSize = BindFunc<_GetRdRamSize>("GetRdRamSize");
                GetRomSize = BindFunc<_GetRomSize>("GetRomSize");
                ReadRdRamBuffer = BindFunc<_ReadRdRamBuffer>("ReadRdRamBuffer");
                ReadRdRam64 = BindFunc<_ReadRdRam64>("ReadRdRam64");
                ReadRdRam32 = BindFunc<_ReadRdRam32>("ReadRdRam32");
                ReadRdRam16 = BindFunc<_ReadRdRam16>("ReadRdRam16");
                ReadRdRam8 = BindFunc<_ReadRdRam8>("ReadRdRam8");
                WriteRdRamBuffer = BindFunc<_WriteRdRamBuffer>("WriteRdRamBuffer");
                WriteRdRam64 = BindFunc<_WriteRdRam64>("WriteRdRam64");
                WriteRdRam32 = BindFunc<_WriteRdRam32>("WriteRdRam32");
                WriteRdRam16 = BindFunc<_WriteRdRam16>("WriteRdRam16");
                WriteRdRam8 = BindFunc<_WriteRdRam8>("WriteRdRam8");
                ReadRomBuffer = BindFunc<_ReadRomBuffer>("ReadRomBuffer");
                ReadRom64 = BindFunc<_ReadRom64>("ReadRom64");
                ReadRom32 = BindFunc<_ReadRom32>("ReadRom32");
                ReadRom16 = BindFunc<_ReadRom16>("ReadRom16");
                ReadRom8 = BindFunc<_ReadRom8>("ReadRom8");
                WriteRomBuffer = BindFunc<_WriteRomBuffer>("WriteRomBuffer");
                WriteRom64 = BindFunc<_WriteRom64>("WriteRom64");
                WriteRom32 = BindFunc<_WriteRom32>("WriteRom32");
                WriteRom16 = BindFunc<_WriteRom16>("WriteRom16");
                WriteRom8 = BindFunc<_WriteRom8>("WriteRom8");
                RefreshDynarec = BindFunc<_RefreshDynarec>("RefreshDynarec");
            }

            private static bool LoadLib(out IntPtr libPtr, string lib)
            {
                if (!NativeLibrary.TryLoad(GetLib(lib), out libPtr)) return false;

                if (NativeLibrary.TryGetExport(libPtr, "PluginStartup", out IntPtr func))
                {
                    var libName = lib.Replace("mupen64plus", "m64p").Replace("-sdl", "");
                    var context = Marshal.StringToHGlobalAnsi(libName);
                    var invoke = Marshal.GetDelegateForFunctionPointer<_PluginStartup>(func);
                    invoke(CorePtr, context, DebugCallback);
                }
                else
                {
                    NativeLibrary.Free(libPtr);
                    return false;
                }

                return true;
            }

            private static void UnloadLib(IntPtr libPtr)
            {
                if (libPtr == default) return;

                if (NativeLibrary.TryGetExport(libPtr, "PluginShutdown", out IntPtr func))
                {
                    var invoke = Marshal.GetDelegateForFunctionPointer<_PluginShutdown>(func);
                    invoke();
                }

                NativeLibrary.Free(libPtr);
            }

            #region Plugins

            /// <summary>
            /// Initializes the plugin
            /// </summary>
            /// <param name="coreHandle">The DLL handle for the core DLL</param>
            /// <param name="context">Giving a context to the DebugCallback</param>
            /// <param name="debugCallback">A function to use when the pluging wants to output debug messages</param>
            [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
            private delegate ErrorCode _PluginStartup(IntPtr coreHandle, IntPtr context, _DebugCallback debugCallback);

            /// <summary>
            /// Cleans up the plugin
            /// </summary>
            [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
            private delegate ErrorCode _PluginShutdown();

            internal static void LoadAudio()
            {
                if (AudioPtr == default) UnloadAudio();
                LoadLib(out AudioPtr, "mupen64plus-audio-sdl");

                if (AudioPtr == default)
                    throw new Exception("No library could be loaded for Audio-Plugin!");
            }

            internal static void UnloadAudio()
            {
                CoreDetachPlugin(PluginType.Audio);
                UnloadLib(AudioPtr);
            }

            internal static void LoadInput()
            {
                if (InputPtr == default) UnloadInput();
                LoadLib(out InputPtr, "mupen64plus-input-sdl");

                if (InputPtr == default)
                    throw new Exception("No library could be loaded for Input-Plugin!");
            }

            internal static void UnloadInput()
            {
                CoreDetachPlugin(PluginType.Input);
                UnloadLib(InputPtr);
            }

            internal static void LoadRsp(bool forceCxd4)
            {
                if (RspPtr == default) UnloadRsp();

                // Load most compatible rsp based on video plugin used
                if (curVideo == 1 || forceCxd4) /* AngryLion */
                    LoadLib(out RspPtr, "mupen64plus-rsp-cxd4");
                else LoadLib(out RspPtr, "mupen64plus-rsp-hle");

                if (RspPtr == default)
                    throw new Exception("No library could be loaded for Rsp-Plugin!");
            }

            internal static void UnloadRsp()
            {
                CoreDetachPlugin(PluginType.Rsp);
                UnloadLib(RspPtr);
            }

            internal static void LoadVideo(int index)
            {
                if (VideoPtr == default) UnloadVideo();

                // Try to load preferred video plugin
                if (LoadLib(out VideoPtr, VideoPlugin[index])) curVideo = index;
                else /* Brute force load until a plugin can load */
                {
                    for (var i = 0; i < VideoPlugin.Length; i++)
                        if (LoadLib(out VideoPtr, VideoPlugin[i]))
                        {
                            curVideo = i;
                            return;
                        }
                }

                if (VideoPtr == default)
                    throw new Exception("No library could be loaded for Video-Plugin!");
            }

            internal static void UnloadVideo()
            {
                CoreDetachPlugin(PluginType.Video);
                UnloadLib(VideoPtr);
            }

            internal static void LoadPlugins(int prefVideo, bool forceCxd4)
            {
                LoadVideo(prefVideo);
                LoadAudio();
                LoadInput();
                LoadRsp(forceCxd4);

                var rval = CoreAttachPlugin(PluginType.Video, VideoPtr);
                if (rval != ErrorCode.Success)
                    throw new Exception("Core error while attaching 'Video' plugin.");

                rval = CoreAttachPlugin(PluginType.Audio, AudioPtr);
                if (rval != ErrorCode.Success)
                    throw new Exception("Core error while attaching 'Audio' plugin.");

                rval = CoreAttachPlugin(PluginType.Input, InputPtr);
                if (rval != ErrorCode.Success)
                    throw new Exception("Core error while attaching 'Input' plugin.");

                rval = CoreAttachPlugin(PluginType.Rsp, RspPtr);
                if (rval != ErrorCode.Success)
                    throw new Exception("Core error while attaching 'Rsp' plugin.");
            }

            internal static void UnloadPlugins()
            {
                if (RspPtr != default) UnloadRsp();
                if (InputPtr != default) UnloadInput();
                if (AudioPtr != default) UnloadAudio();
                if (VideoPtr != default) UnloadVideo();
            }

            #endregion
        }
    }
}