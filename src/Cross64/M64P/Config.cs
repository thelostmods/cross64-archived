﻿using System;
using System.Runtime.InteropServices;

namespace Cross64
{
    internal static partial class M64P
    {
        /// <summary>
        /// Opens a section in the global config system
        /// </summary>
        /// <param name="sectionName">The name of the section to open</param>
        /// <param name="configSectionHandle">A pointer to the pointer to use as the section handle</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigOpenSection(string sectionName, out IntPtr configSectionHandle);
        public static _ConfigOpenSection ConfigOpenSection = null!;
        
        /// <summary>
        /// Deletes a section in the global config system
        /// </summary>
        /// <param name="sectionName">The name of the section to delete</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigDeleteSection(string sectionName);
        public static _ConfigDeleteSection ConfigDeleteSection = null!;

        /// <summary>
        /// Saves a section in the global config system
        /// </summary>
        /// <param name="sectionName">The name of the section to save</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSaveSection(string sectionName);
        public static _ConfigSaveSection ConfigSaveSection = null!;
        
        /// <summary>
        /// Saves the entire global config system
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSaveFile();
        public static _ConfigSaveFile ConfigSaveFile = null!;

        
        /// <summary>
        /// Sets a parameter default value in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be an float)</param>
        /// <param name="paramComment">A description of the parameter</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetDefaultFloat(IntPtr configSectionHandle, string paramName, float paramValue, string paramComment);
        public static _ConfigSetDefaultFloat ConfigSetDefaultFloat = null!;

        /// <summary>
        /// Sets a parameter default value in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be an int)</param>
        /// <param name="paramComment">A description of the parameter</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetDefaultInt(IntPtr configSectionHandle, string paramName, int paramValue, string paramComment);
        public static _ConfigSetDefaultInt ConfigSetDefaultInt = null!;

        /// <summary>
        /// Sets a parameter default value in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be a string)</param>
        /// <param name="paramComment">A description of the parameter</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetDefaultString(IntPtr configSectionHandle, string paramName, string paramValue, string paramComment);
        public static _ConfigSetDefaultString ConfigSetDefaultString = null!;
        
        /// <summary>
        /// Gets a parameter from the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to get</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate float _ConfigGetParamFloat(IntPtr configSectionHandle, string paramName);
        public static _ConfigGetParamFloat ConfigGetParamFloat = null!;

        /// <summary>
        /// Gets a parameter from the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to get</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int _ConfigGetParamInt(IntPtr configSectionHandle, string paramName);
        public static _ConfigGetParamInt ConfigGetParamInt = null!;

        /// <summary>
        /// Gets a parameter from the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to get</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate string _ConfigGetParamString(IntPtr configSectionHandle, string paramName);
        public static _ConfigGetParamString ConfigGetParamString = null!;
        
        #region Templates (Not used directly)
        
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetParameterFloat(IntPtr configSectionHandle, string paramName, TypeDef paramType, float paramValue);
        public static _ConfigSetParameterFloat ConfigSetParameterFloat = null!;
        
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetParameterInt(IntPtr configSectionHandle, string paramName, TypeDef paramType, int paramValue);
        public static _ConfigSetParameterInt ConfigSetParameterInt = null!;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _ConfigSetParameterString(IntPtr configSectionHandle, string paramName, TypeDef paramType, string paramValue);
        public static _ConfigSetParameterString ConfigSetParameterString = null!;

        #endregion

        /// <summary>
        /// Sets a parameter in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramType">The type of the parameter</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be an float)</param>
        public static ErrorCode ConfigSetParamFloat(IntPtr configSectionHandle, string paramName, float paramValue)
        {
            return ConfigSetParameterFloat(configSectionHandle, paramName, TypeDef.Float, paramValue);
        }

        /// <summary>
        /// Sets a parameter in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramType">The type of the parameter</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be an int)</param>
        public static ErrorCode ConfigSetParamInt(IntPtr configSectionHandle, string paramName, int paramValue)
        {
            return ConfigSetParameterFloat(configSectionHandle, paramName, TypeDef.Int, paramValue);
        }

        /// <summary>
        /// Sets a parameter in the global config system
        /// </summary>
        /// <param name="configSectionHandle">The handle of the section to access</param>
        /// <param name="paramName">The name of the parameter to set</param>
        /// <param name="paramType">The type of the parameter</param>
        /// <param name="paramValue">A pointer to the value to use for the parameter (must be a string)</param>
        public static ErrorCode ConfigSetParamString(IntPtr configSectionHandle, string paramName, string paramValue)
        {
            return ConfigSetParameterString(configSectionHandle, paramName, TypeDef.String, paramValue);
        }
        
        //public delegate void ConfigListSectionsCallback(IntPtr ptr, string s);
        //public delegate void ConfigListParametersCallback(IntPtr ptr, string s, TypeDef t);
        //public static Func<IntPtr, ConfigListSectionsCallback, ErrorCode>? ConfigListSections;
        //public static Func<IntPtr, IntPtr, ConfigListParametersCallback, ErrorCode>? ConfigListParameters;
        //public static Func<IntPtr, string, TypeDef, ErrorCode>? ConfigGetParameterType;
        //public static Func<IntPtr, string, ErrorCode>? ConfigGetParameterHelp;
    }
}