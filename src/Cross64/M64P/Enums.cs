﻿namespace Cross64
{
    internal static partial class M64P
    {
        public enum Command : int
        {
            Nop = 0,
            RomOpen,
            RomClose,
            RomGetHeader,
            RomGetSettings,
            Execute,
            Stop,
            Pause,
            Resume,
            CoreStateQuery,
            StateLoad,
            StateSave,
            StateSetSlot,
            SendSdlKeyDown,
            SendSdlKeyUp,
            SetFrameCallback,
            TakeNextScreenshot,
            CoreStateSet,
            ReadScreen,
            Reset,
            AdvanceFrame,
            SetMediaLoader,
            NetplayInit,
            NetplayControlPlayer,
            NetplayGetVersion,
            NetplayClose
        }

        public enum CoreParam : int
        {
            EmuState = 1,
            VideoMode,
            SaveStateSlot,
            SpeedFactor,
            SpeedLimiter,
            VideoSize,
            AudioVolume,
            AudioMute,
            InputGameshark,
            StateLoadComplete,
            StateSaveComplete
        }

        public enum ErrorCode : int
        {
            Success = 0,
            NotInit, /* Function is disallowed before InitMupen64Plus() is called */
            AlreadyInit, /* InitMupen64Plus() was called twice */
            Incompatible, /* API versions between components are incompatible */
            InputAssert, /* Invalid parameters for function call, such as ParamValue=NULL for GetCoreParameter() */
            InputInvalid, /* Invalid input data, such as ParamValue="maybe" for SetCoreParameter() to set a BOOL-type value */
            InputNotFound, /* The input parameter(s) specified a particular item which was not found */
            NoMemory, /* Memory allocation failed */
            Files, /* Error opening, creating, reading, or writing to a file */
            Internal, /* Internal error (bug) */
            InvalidState, /* Current program state does not allow operation */
            PluginFail, /* A plugin function returned a fatal error */
            SystemFail, /* A system function call, such as an SDL or file operation, failed */
            Unsupported, /* Function call is not supported (ie, core not built with debugger) */
            WrongType /* A given input type parameter cannot be used for desired operation */
        }

        public enum GL_Attribute : int
        {
            DoubleBuffer = 1,
            BufferSize,
            DepthSize,
            RedSize,
            GreenSize,
            BlueSize,
            AlphaSize,
            SwapControl,
            MultiSampleBuffers,
            MultiSampleSamples,
            ContextMajorVersion,
            ContextMinorVersion,
            ContextProfileMask
        }

        public enum MsgType : int
        {
            Error = 1,
            Warning,
            Info,
            Status,
            Verbos
        }

        public enum PluginType : int
        {
            Null = 0,
            Rsp,
            Video,
            Audio,
            Input,
            Core
        }

        public enum TypeDef : int
        {
            Int = 1,
            Float,
            Bool,
            String
        }

        public enum VideoFlags : int
        {
            SupportResizing = 1
        }

        public enum VideoMode : int
        {
            None = 1,
            Windowed,
            FullScreen
        }
    }
}