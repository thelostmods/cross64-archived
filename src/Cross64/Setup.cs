using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace Cross64
{
    internal static partial class Setup
    {
#if LINUX || OSX
        private static string GetLib(string name)
        {
            var path = $"{Environment.CurrentDirectory}/emulator/";

#if LINUX
            return $"{path}{name}.so";
#elif OSX
            return $"{path}{name}.dylib";
#endif
        }

        public static void Verify()
        {
            IntPtr testPtr;

            // Test ability to load the binary
            if (!NativeLibrary.TryLoad(GetLib("mupen64plus"), out testPtr))
                throw new Exception(
                    "Mupen64Plus-Core or its dependencies could not be loaded!" +
                    "Please use a package manager to install the official mupen64plus (2.5.0 or higher) properly and " +
                    "ask for help in the support channel on the official Cross64 Discord server if problems persist!");

            // Succeeded, unload and proceed as normal
            NativeLibrary.Free(testPtr);
        }
#endif
        
#if WINDOWS
        public static void Verify()
        {
            if (CheckRuntimes()) return;

            InstallRuntimes();

            if (!CheckRuntimes())
                throw new Exception("Mupen dependancies were not installed. Exiting program!");
        }

        private static bool CheckRuntimes()
        {
            if (IntPtr.Size == 8 || !Directory.Exists("C:/Windows/SysWOW64"))
                return File.Exists("C:/Windows/System32/vcruntime140.dll");
            else
                return File.Exists("C:/Windows/SysWOW64/vcruntime140.dll");
        }

        private static void InstallRuntimes()
        {
            Console.WriteLine("Mupen dependencies need installed.");

            var path = $"{Environment.CurrentDirectory}/vc_redist.exe";
            var link = IntPtr.Size == 8
                ?
                /*64-bit*/ "https://aka.ms/vs/16/release/vc_redist.x64.exe"
                :
                /*32-bit*/ "https://aka.ms/vs/16/release/vc_redist.x86.exe";

            // Download Runtimes
            if (!File.Exists(path))
                try
                {
                    var request = System.Net.WebRequest.Create(link);
                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var fSize = response.ContentLength;
                            const int bSize = 1024 * 1024;
                            var buf = new byte[bSize];

                            using (var f = new FileStream(path, FileMode.OpenOrCreate))
                            {
                                var len = stream.Read(buf, 0, bSize);

                                while (len > 0)
                                {
                                    f.Write(buf, 0, len);

                                    var progress = (int) (((float) f.Length / fSize) * 100);
                                    Console.Write(
                                        $"\rDownloading VC++ 2019 Runtimes: {progress}%"); // Needs replaced by gui solution eventually

                                    len = stream.Read(buf, 0, bSize);
                                    System.Threading.Thread.Sleep(1);
                                }

                                Console.WriteLine();
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    throw new Exception("Failed to fetch required runtimes for Mupen to run!" +
                                        "\nReason: " + err);
                }

            // Install runtimes
            var p = Process.Start(path);
            p.WaitForInputIdle();
            p.WaitForExit();
        }
#endif
    }
}