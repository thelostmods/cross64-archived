namespace Cross64.Runtimes
{
    public static partial class Utility
    {
        public static uint BSwap32(uint value)
        {
            return
                ((value & 0xff000000) >> 24) |
                ((value & 0x00ff0000) >> 8) |
                ((value & 0x0000ff00) << 8) |
                ((value & 0x000000ff) << 24);
        }

        public static ulong BSwap64(ulong value)
        {
            return ((ulong) BSwap32((uint) (value >> 32)) << 32) | BSwap32((uint) value);
        }
    }
}