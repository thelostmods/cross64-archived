﻿using System;

namespace Cross64.Runtimes
{
    public static partial class Utility
    {
        public class Vector2H
        {
            public short X = 0;
            public short Y = 0;

            public Vector2H()
            {
            }

            public Vector2H(Vector2H vector)
            {
                X = vector.X;
                Y = vector.Y;
            }

            public Vector2H(byte[] buffer)
            {
                var size = buffer.Length / 2;
                if (size < 2) return;

                X = BitConverter.ToInt16(buffer, 0);
                Y = BitConverter.ToInt16(buffer, size);
            }

            public Vector2H(short x, short y)
            {
                X = x;
                Y = y;
            }

            public byte[] ToArray()
            {
                var ret = new byte[4];
                Buffer.BlockCopy(BitConverter.GetBytes(X), 0, ret, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(Y), 0, ret, 2, 2);
                return ret;
            }

            public override string ToString()
            {
                return $@"Vector2H[{X.ToString()}, {Y.ToString()}]";
            }

            public string ToString(string format)
            {
                return $@"Vector2H[{X.ToString(format)}, {Y.ToString(format)}]";
            }

            protected bool Equals(Vector2H other)
            {
                return X == other.X && Y == other.Y;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Vector2H) obj);
            }

            public static bool operator ==(Vector2H a, Vector2H b)
            {
                return a.X == b.X && a.Y == b.Y;
            }

            public static bool operator !=(Vector2H a, Vector2H b)
            {
                return a.X != b.X || a.Y != b.Y;
            }

            public static Vector2H operator +(Vector2H a, Vector2H b)
            {
                return new((short) (a.X + b.X), (short) (a.Y + b.Y));
            }

            public static Vector2H operator -(Vector2H a, Vector2H b)
            {
                return new((short) (a.X - b.X), (short) (a.Y - b.Y));
            }

            public override int GetHashCode()
            {
                return X.GetHashCode() + Y.GetHashCode();
            }
        }

        public class Vector2
        {
            public float X = 0;
            public float Y = 0;

            public Vector2()
            {
            }

            public Vector2(Vector2 vector)
            {
                X = vector.X;
                Y = vector.Y;
            }

            public Vector2(byte[] buffer)
            {
                var size = buffer.Length / 2;
                if (size < 4) return;

                var x = BitConverter.ToUInt32(buffer, 0);
                var y = BitConverter.ToUInt32(buffer, size);

                X = BitConverter.Int32BitsToSingle((int) BSwap32(x));
                Y = BitConverter.Int32BitsToSingle((int) BSwap32(y));
            }

            public Vector2(float x, float y)
            {
                X = x;
                Y = y;
            }

            public byte[] ToArray()
            {
                var ret = new byte[8];
                Buffer.BlockCopy(BitConverter.GetBytes(X), 0, ret, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(Y), 0, ret, 4, 4);
                return ret;
            }

            public override string ToString()
            {
                return $@"Vector2[{X.ToString()}, {Y.ToString()}]";
            }

            public string ToString(string format)
            {
                return $@"Vector2[{X.ToString(format)}, {Y.ToString(format)}]";
            }

            protected bool Equals(Vector2 other)
            {
                return X.Equals(other.X) && Y.Equals(other.Y);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Vector2) obj);
            }

            public static bool operator ==(Vector2 a, Vector2 b)
            {
                return a.X == b.X && a.Y == b.Y;
            }

            public static bool operator !=(Vector2 a, Vector2 b)
            {
                return a.X != b.X || a.Y != b.Y;
            }

            public static Vector2 operator +(Vector2 a, Vector2 b)
            {
                return new(a.X + b.X, a.Y + b.Y);
            }

            public static Vector2 operator -(Vector2 a, Vector2 b)
            {
                return new(a.X - b.X, a.Y - b.Y);
            }

            public override int GetHashCode()
            {
                return X.GetHashCode() + Y.GetHashCode();
            }
        }

        public class Vector3H
        {
            public short X = 0;
            public short Y = 0;
            public short Z = 0;

            public Vector3H()
            {
            }

            public Vector3H(Vector3H vector)
            {
                X = vector.X;
                Y = vector.Y;
                Z = vector.Z;
            }

            public Vector3H(byte[] buffer)
            {
                var size = buffer.Length / 3;
                if (size < 2) return;

                X = BitConverter.ToInt16(buffer, 0);
                Y = BitConverter.ToInt16(buffer, size);
                Z = BitConverter.ToInt16(buffer, size * 2);
            }

            public Vector3H(short x, short y, short z)
            {
                X = x;
                Y = y;
                Z = z;
            }

            public byte[] ToArray()
            {
                var ret = new byte[6];
                Buffer.BlockCopy(BitConverter.GetBytes(X), 0, ret, 0, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(Y), 0, ret, 2, 2);
                Buffer.BlockCopy(BitConverter.GetBytes(Z), 0, ret, 4, 2);
                return ret;
            }

            public override string ToString()
            {
                return $@"Vector3H[{X.ToString()}, {Y.ToString()}, {Z.ToString()}]";
            }

            public string ToString(string format)
            {
                return $@"Vector3H[{X.ToString(format)}, {Y.ToString(format)}, {Z.ToString(format)}]";
            }

            protected bool Equals(Vector3H other)
            {
                return X == other.X && Y == other.Y && Z == other.Z;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Vector3H) obj);
            }

            public static bool operator ==(Vector3H a, Vector3H b)
            {
                return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
            }

            public static bool operator !=(Vector3H a, Vector3H b)
            {
                return a.X != b.X || a.Y != b.Y || a.Z != b.Z;
            }

            public static Vector3H operator +(Vector3H a, Vector3H b)
            {
                return new((short) (a.X + b.X), (short) (a.Y + b.Y), (short) (a.Z + b.Z));
            }

            public static Vector3H operator -(Vector3H a, Vector3H b)
            {
                return new((short) (a.X - b.X), (short) (a.Y - b.Y), (short) (a.Z - b.Z));
            }

            public override int GetHashCode()
            {
                return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
            }
        }

        public class Vector3
        {
            public float X = 0;
            public float Y = 0;
            public float Z = 0;

            public Vector3()
            {
            }

            public Vector3(Vector3 vector)
            {
                X = vector.X;
                Y = vector.Y;
                Z = vector.Z;
            }

            public Vector3(byte[] buffer)
            {
                var size = buffer.Length / 3;
                if (size < 4) return;

                var x = BitConverter.ToUInt32(buffer, 0);
                var y = BitConverter.ToUInt32(buffer, size);
                var z = BitConverter.ToUInt32(buffer, size * 2);

                X = BitConverter.Int32BitsToSingle((int) BSwap32(x));
                Y = BitConverter.Int32BitsToSingle((int) BSwap32(y));
                Z = BitConverter.Int32BitsToSingle((int) BSwap32(z));
            }

            public Vector3(float x, float y, float z)
            {
                X = x;
                Y = y;
                Z = z;
            }

            public static Vector3 Forward(float rotationX, float rotationY)
            {
                var cosY = (float) Math.Cos(rotationY);
                var x = cosY * (float) Math.Sin(rotationX);
                var y = (float) Math.Sin(rotationY);
                var z = cosY * (float) Math.Cos(rotationX);
                return new(x, y, z);
            }

            public byte[] ToArray()
            {
                var ret = new byte[12];
                Buffer.BlockCopy(BitConverter.GetBytes(X), 0, ret, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(Y), 0, ret, 4, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(Z), 0, ret, 8, 4);
                return ret;
            }

            public override string ToString()
            {
                return $@"Vector3[{X.ToString()}, {Y.ToString()}, {Z.ToString()}]";
            }

            public string ToString(string format)
            {
                return $@"Vector3[{X.ToString(format)}, {Y.ToString(format)}, {Z.ToString(format)}]";
            }

            protected bool Equals(Vector3 other)
            {
                return X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Vector3) obj);
            }

            public static bool operator ==(Vector3 a, Vector3 b)
            {
                return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
            }

            public static bool operator !=(Vector3 a, Vector3 b)
            {
                return a.X != b.X || a.Y != b.Y || a.Z != b.Z;
            }

            public static Vector3 operator +(Vector3 a, Vector3 b)
            {
                return new(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
            }

            public static Vector3 operator -(Vector3 a, Vector3 b)
            {
                return new(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            }

            public override int GetHashCode()
            {
                return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
            }
        }
    }
}