﻿using System;

namespace Cross64
{
    internal abstract class ScreenBase : IDisposable
    {
        public abstract void Initialize();

        public abstract void Update();

        public abstract void Draw();

        protected abstract void Destroy();

        public void Dispose() { Destroy(); }
    }
}