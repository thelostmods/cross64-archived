﻿using Cross64.Runtimes;

namespace Sm64Online
{
    public partial class Sm64OnlineMod
    {

        private void HandleSceneChange(int scene)
        {
            //if (scene === this.curScene) return;

            //// Set global to current scene value
            //this.curScene = scene;

            //this.ModLoader.clientSide.sendPacket(new Net.SyncNumber(this.ModLoader.clientLobby, "SyncScene", scene, true));
            //this.ModLoader.logger.info('Moved to scene[' + scene + '].');
        }

        private void HandlePuppets(int scene, bool visible)
        {
            if (Emulator.RdRam.Read32(0x8033EFFC) < 50) return;

            pMgr.scene = scene;
            pMgr.onTick(this.curScene != -1 && visible);
        }

        private void HandleSaveFlats(byte[] bufData, byte[] bufStorage, int profile)
        {
            //// Initializers
            //let pData: Net.SyncBuffered;
            //let i: number;
            //let count: number;
            //let needUpdate = false;

            //bufData = this.core.save[profile].get_all();
            //bufStorage = this.cDB.save_data;
            //count = bufData.byteLength;
            //needUpdate = false;

            //for (i = 0; i < count; i++)
            //{
            //	if (bufData[i] === bufStorage[i]) continue;

            //	bufData[i] |= bufStorage[i];
            //	this.core.save[profile].set(i, bufData[i]);
            //	needUpdate = true;

            //	this.ModLoader.log("The byte was " + i + ". If the hat glitch occured when this sync'd, please report to spiceywolf!");
            //}

            //// Send Changes to Server
            //if (!needUpdate) return;
            //this.cDB.save_data = bufData;
            //pData = new Net.SyncBuffered(this.ModLoader.clientLobby, 'SyncSaveFile', bufData, false);
            //this.ModLoader.clientSide.sendPacket(pData);
        }

        private void HandleStarCount()
        {
            //// Initializers
            //let pData: Net.SyncNumber;
            //let val: number;
            //let valDB: number;
            //let needUpdate = false;

            //val = this.core.runtime.star_count;
            //valDB = this.cDB.star_count;

            //// Detect Changes
            //if (val === valDB) return;

            //// Process Changes
            //if (val > valDB)
            //{
            //	this.cDB.star_count = val;
            //	needUpdate = true;
            //}
            //else
            //{
            //	this.core.runtime.star_count = valDB;
            //}

            //// Send Changes to Server
            //if (!needUpdate) return;
            //pData = new Net.SyncNumber(this.ModLoader.clientLobby, 'SyncStarCount', val, false);
            //this.ModLoader.clientSide.sendPacket(pData);
        }
    }
}
