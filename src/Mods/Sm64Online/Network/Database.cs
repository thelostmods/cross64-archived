using Cross64.Api;

namespace Sm64Online
{
    public partial class Sm64OnlineMod
    {
        private class Database
        {
            public byte[] SaveData;
            public int StarCount;

            public Database(SuperMario64 api)
            {
                SaveData = new byte[api.SaveDataSize];
            }
        }

        private class DatabaseClient : Database
        {
            public DatabaseClient(SuperMario64 api) : base(api)
            {
            }
        }

        private class DatabaseServer : Database
        {
            // Puppets
            // playerInstances: any = {};
            // players: any = {};
            
            public DatabaseServer(SuperMario64 api) : base(api)
            {
            }
        }
    }
}