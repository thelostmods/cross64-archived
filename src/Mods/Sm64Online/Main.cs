using System;
using Cross64;
using Cross64.Api;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Sm64Online
{
    public class Sm64OnlinePlugin : PluginBase
    {
        public override NetworkType NetworkMode => NetworkType.Decentralized;
        public override string MasterServerIp => "158.69.60.101";
        public override GameID[] Game => new[] {GameID.SuperMario64};
        public override string Name => "Sm64Online";
        public override string Description => "Made by SpiceyWolf and MelonSpeedrun.";
        public override string DiscordImage => "sm64o";
        public override ModBase InitMod => new Sm64OnlineMod();
    }

    public partial class Sm64OnlineMod : NetworkedModBase
    {
        protected override object Config => config;
        private ConfigDef config = new ConfigDef();

        public class ConfigDef
        {
            public bool PrintNetClient { get; set; } = false;
            public bool PrintNetServer { get; set; } = false;
        }

        internal SuperMario64 Api;

        private DatabaseClient dbClient;
        private PuppetManager pMgr;

        private int curScene = -1;
        private bool isPaused = false;

        public override void Initialize(ApiBase api)
        {
            Api = (SuperMario64) api;
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override void OnFirstFrame()
        {
            // Clone old game-logic function to hook space (Romhack compatibility)
            var funLogic = Emulator.RdRam.ReadB(0x2CB1C0, 0x1000);
            Emulator.RdRam.WriteB(0x801000, funLogic);

            // Inject puppet behavior
            Emulator.RdRam.Write32(0x800000, 0x00000000);
            Emulator.RdRam.Write32(0x800004, 0x11012001);
            Emulator.RdRam.Write32(0x800008, 0x23000000);
            Emulator.RdRam.Write32(0x80000C, 0x002500A0);
            Emulator.RdRam.Write32(0x800010, 0x2F000000);
            Emulator.RdRam.Write32(0x800014, 0x00000000);
            Emulator.RdRam.Write32(0x800018, 0x11030001);
            Emulator.RdRam.Write32(0x80001C, 0x08000000);
            Emulator.RdRam.Write32(0x800020, 0x10050000);
            Emulator.RdRam.Write32(0x800024, 0x101D0000);
            Emulator.RdRam.Write32(0x800028, 0x09000000);

            Utility.LoadGameshark(Utility.GetGameshark(this, "E0/Puppets"));
            Utility.LoadGameshark(Utility.GetGameshark(this, "E0/Hooks"), false);
        }

        public override void OnTick(uint frame)
        {
            SetMarioExists();

            if (MarioExists)
            {
                waiter += 1;
                if (waiter < 1000) return;
                if (!isSet)
                {
                    Emulator.RdRam.Write32(0x803000, 0xffffffff);
                    isSet = true;
                }
                else
                {
                    Emulator.Pointer puppet = Emulator.RdRam.Read32(0x803004) & 0xffffff;

                    if (puppet != 0)
                    {
                        Console.WriteLine("Puppet Addr: " + puppet.ToString("X"));
                        puppet.Dereference();
                        Console.WriteLine("PuppetVal: " + puppet.U32.ToString("X"));

                        var pos = Api.Player.Position;
                        var pre = (puppet + 0xa0).ReadB(12);

                        // for (var i = 0; i < 12; i ++) 
                        //     Console.Write(pre[i].ToString("X"));
                        // Console.WriteLine();
                        //
                        // Console.WriteLine("Visible: " + ((puppet + 0x02).U16).ToString("X"));
                        //
                        // Emulator.RdRam.WriteBuffer(puppet + 0xA0, pos);
                        // Emulator.RdRam.Write16(puppet + 0x02, 0x21);
                    }
                    else
                    {
                        Console.WriteLine("Someting went wong");
                        isSet = false;
                        waiter = 0;
                    }
                }
            }
        }

        public override void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        private int waiter = 0;
        private bool isSet = false;
        private bool MarioExists = true;

        private void SetMarioExists()
        {
            if (Api.Player.Exists != MarioExists)
                MarioExists = Api.Player.Exists;

            Discord.SetDescription("Mario Exists = " + MarioExists);
        }

        public override void Client_JoinLobby()
        {
            Console.WriteLine("I joined a lobby");
            foreach (var p in Players.Keys)
            {
                if (p != MyIndex) Console.WriteLine("Player: " + Players[p] + " was already in here!");
            }
        }

        public override void Client_PlayerJoined(int index)
        {
            Console.WriteLine("Player: " + Players[index] + " has joined!");
        }

        public override void Client_PlayerLeft(int index)
        {
        }

        public override void Server_LobbyCreated()
        {
        }

        public override void Server_PlayerJoined(int index)
        {
        }

        public override void Server_PlayerLeft(int index)
        {
        }
    }
}