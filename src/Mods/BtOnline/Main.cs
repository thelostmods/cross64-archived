using System;
using Cross64;
using Cross64.Api;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace BtOnline
{
    public class Sm64OnlinePlugin : PluginBase
    {
        public override NetworkType NetworkMode => NetworkType.Decentralized;
        public override string MasterServerIp => "158.69.60.101";
        public override GameID[] Game => new[] {GameID.BanjoTooie};
        public override string Name => "BtOnline";
        public override string Description => "Made by PMONickPop123, SpiceyWolf and Wedarobi.";
        public override string DiscordImage => "bto";
        public override ModBase InitMod => new BtOnlineMod();
    }

    public partial class BtOnlineMod : NetworkedModBase
    {
        protected override object Config => config;
        private ConfigDef config = new ConfigDef();

        public class ConfigDef
        {
            public bool PrintEventsLevel { get; set; } = false;
            public bool PrintEventsScene { get; set; } = false;
            public bool PrintNetClient { get; set; } = false;
            public bool PrintNetServer { get; set; } = false;
            public bool ShowTracker { get; set; } = false;
            public bool SkipIntro { get; set; } = false;
        }

        internal BanjoTooie Api;

        public override void Initialize(ApiBase api)
        {
            Api = (BanjoTooie) api;
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override void OnTick(uint frame)
        {
        }

        public override void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void Client_JoinLobby()
        {
        }

        public override void Client_PlayerJoined(int index)
        {
        }

        public override void Client_PlayerLeft(int index)
        {
        }

        public override void Server_LobbyCreated()
        {
        }

        public override void Server_PlayerJoined(int index)
        {
        }

        public override void Server_PlayerLeft(int index)
        {
        }
    }
}