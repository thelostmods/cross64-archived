using System;
using Cross64;
using Cross64.Api;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace CbfdOnline
{
    public class Sm64OnlinePlugin : PluginBase
    {
        public override NetworkType NetworkMode => NetworkType.Decentralized;
        public override string MasterServerIp => "158.69.60.101";
        public override GameID[] Game => new[] {GameID.ConkersBadFurDay};
        public override string Name => "CbfdOnline";
        public override string Description => "Made by SpiceyWolf and Seohaine.";
        public override string DiscordImage => "cbfdo";
        public override ModBase InitMod => new CbfdOnlineMod();
    }

    public partial class CbfdOnlineMod : NetworkedModBase
    {
        protected override object Config => config;
        private ConfigDef config = new ConfigDef();

        public class ConfigDef
        {
            public bool PrintNetClient { get; set; } = false;
            public bool PrintNetServer { get; set; } = false;
        }

        internal ConkersBadFurDay Api;

        public override void Initialize(ApiBase api)
        {
            Api = (ConkersBadFurDay) api!;
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override void OnTick(uint frame)
        {
        }
        
        public override void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void Client_JoinLobby()
        {
        }

        public override void Client_PlayerJoined(int index)
        {
        }

        public override void Client_PlayerLeft(int index)
        {
        }

        public override void Server_LobbyCreated()
        {
        }

        public override void Server_PlayerJoined(int index)
        {
        }

        public override void Server_PlayerLeft(int index)
        {
        }
    }
}